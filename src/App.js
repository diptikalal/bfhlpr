import React from 'react';
import { Switch } from 'react-router-dom';
import { renderRoutes } from 'react-router-config';

import Routes from './Routes/Routes';
import './App.css';

function App() {
  return (
    <main className='app'>
      <Switch>
        {
          renderRoutes(Routes)
        }
      </Switch>
    </main>
  );
}

export default App;
