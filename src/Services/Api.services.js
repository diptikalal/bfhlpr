// Extend base class to make specific APIs.

import BaseServices from './Base.services';
import { HTTP_METHODS } from '../App.constants';

class ApiServices extends BaseServices {
    //  CREATE
    add = (
        todo = {
            title: 'foo',
            body: 'bar',
            userId: 1
        }
    ) => {
        const {
            POST: {
                contentType,
                method
            }
        } = HTTP_METHODS;
        return fetch(
            this.url, 
            {
                method,
                headers: {
                    'Content-type': contentType
                },
                body: JSON.stringify(todo)
            }
        )
        .then(this.toJSON)
        .catch(this.toError);
    }


    //  READ
    get = () => 
        fetch(this.url)
        .then(this.toJSON)
        .catch(this.toError);


    //  UPDATE
    modify = (
        updatedTodo = {
            title: 'foo'
        }
    ) => {
        const {
            PATCH: {
                contentType,
                method
            }
        } = HTTP_METHODS;
        return fetch(
            this.url, 
            {
                method,
                headers: {
                    'Content-type': contentType
                },
                body: JSON.stringify(updatedTodo)
            }
        )
        .then(this.toJSON)
        .catch(this.toError);
    }


    //  DELETE
    remove = () => {
        const {
            DELETE: {
                method
            }
        } = HTTP_METHODS;
        return fetch(
            this.url,
            {
                method
            }
        )
        .then(this.toJSON)
        .catch(this.toError);
    }
}
// get your own key from unsplash please 😇
const KEY =
    '?client_id=5f96323678d05ff0c4eb264ef184556868e303b32a2db88ecbf15746e6f25e02';
const URL = `https://api.unsplash.com/photos/`;
const REVIEWAPI = `https://bfhl-dev.extentia.com/index.php/wp-json/wp/v2/pages/home`

const fetchImages = async page => {
    const response = await fetch(`${URL}${KEY}&per_page=3&page=${page}`);
    const data = await response.json();
    if (response.status >= 400) {
        throw new Error(data.errors);
    }
    return data;
};

const fetchReviews = () => {
    const response =  fetch(`${REVIEWAPI}`)
    .then(response => response.json() ) ;
    if (response.status >= 400) {
        throw new Error(response.json.errors);
    }
    return response;
};

const fetchImageStats = async id => {
    const response = await fetch(`${URL}/${id}/statistics${KEY}`);
    const data = await response.json();
    if (response.status >= 400) {
        throw new Error(data.errors);
    }
    return data;
};

export { fetchImages, fetchImageStats,fetchReviews  };

const newApiServices = url => new ApiServices(`${process.env.REACT_APP_API_ENDPOINT}${url}`);

export default newApiServices;