//Base class of HTTP here
import { FETCH_ERROR_TEXT } from '../App.constants';

class BaseServices {
    constructor(url) {
        this.url = url;
    }

    toJSON = response => {
        if (!response.ok) {
            this.handleResponseError(response);
        }
        return response.json();
    };

    getFetchErrorText = status => `${FETCH_ERROR_TEXT} ${status}`;

    handleResponseError({ status }) {
        throw new Error(this.getFetchErrorText(status));
    }

    toError = error => {
        console.error(error);
        return error;
    };
}

export default BaseServices;