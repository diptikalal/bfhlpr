const FETCH_ERROR_TEXT = 'FETCH error, status =';
const HTTP_METHODS = {
    DELETE: {
        method: 'POST'
    },
    PATCH: {
        contentType: 'application/json; charset=UTF-8',
        method: 'PATCH'
    },
    POST: {
        contentType: 'application/json; charset=UTF-8',
        method: 'POST'
    },
};

export { 
    FETCH_ERROR_TEXT,
    HTTP_METHODS
};