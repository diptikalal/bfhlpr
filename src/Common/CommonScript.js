// Only write pure functions over here.

const addNewEventListener = (elementNode, eventName, callback) => {
    elementNode.addEventListener(eventName, callback);
    return elementNode;
};

const removeExistingEventListener = (elementNode, eventName, callback) => {
    elementNode.removeEventListener(eventName, callback);
    return elementNode;
};

export {
    addNewEventListener,
    removeExistingEventListener
};