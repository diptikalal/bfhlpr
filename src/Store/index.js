import { createStore, applyMiddleware } from 'redux';
import Reducer from './Helper/Reducers';
import sagaMiddleware from './Helper/Middleware';
import rootSaga from './Helper/Sagas';
import { composeWithDevTools } from 'redux-devtools-extension';


const configureStore = () => {
    const store = createStore(
        Reducer,
        composeWithDevTools(applyMiddleware(sagaMiddleware)),
      );
    sagaMiddleware.run(rootSaga);
    return store;
};

export default configureStore;
