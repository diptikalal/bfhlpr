import { combineReducers } from 'redux';

import AboutUsReducer from '../../Pages/AboutUs/AboutUs-reducer/AboutUs.reducer';
import LoginReducer from '../../Pages/Login/Login-reducers/Login.reducer';
import PageReducer from '../../Pages/Login/Login-reducers/Page.reducer';
import ReviewReducer from '../../Modules/Reviews/Reviews-reducers/Reviews.reducer';

export default combineReducers({
    AboutUs: AboutUsReducer,
    Login: LoginReducer,
    nextPage: PageReducer,
    Reviews: ReviewReducer
})
