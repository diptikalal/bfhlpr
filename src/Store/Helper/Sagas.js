import { all } from 'redux-saga/effects';

import imagesSaga from '../../Pages/Login/Login-saga/Login.saga';
import aboutUsSaga from '../../Pages/AboutUs/AboutUs-saga/AboutUs.saga';
import ReviewsSaga from '../../Modules/Reviews/Reviews-saga/Reviews.saga';

export default function* rootSaga() {
    yield all([
        aboutUsSaga(),
        imagesSaga(),
        ReviewsSaga()
    ]);
}
