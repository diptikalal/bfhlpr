import React, { PureComponent } from "react";

import PropTypes from "prop-types";
import { Box, Link } from '@material-ui/core';
import { EmailOutlined } from '@material-ui/icons';

class EmailAddress extends PureComponent {
    render() {
        const {
            className,
            color,
            component,
            emailAddress,
            pl,
            style,
            variant
        } = this.props;

        return (
            <Link
                className={className}
                color={color[0]}
                href={`mailto:${emailAddress}`}
                variant={variant}
            >
                <EmailOutlined
                    style={style}
                    color={color[1]}
                />
                <Box
                    pl={pl}
                    component={component}
                >
                    {emailAddress}
                </Box>
            </Link>
        );
    }
}

EmailAddress.propTypes = {
    className: PropTypes.string,
    color: PropTypes.arrayOf(PropTypes.string),
    component: PropTypes.string,
    emailAddress: PropTypes.string.isRequired,
    pl: PropTypes.number,
    style: PropTypes.shape({
        fontSize: PropTypes.number
    }),
    variant: PropTypes.string
};

EmailAddress.defaultProps = {
    className: "align-items-center",
    color: ["inherit", "secondary"],
    component: "span",
    fontSize: "2rem",
    pl: 3,
    style: { fontSize: 34 },
    variant: "caption"
};

export default EmailAddress;