import React, { PureComponent } from "react";

import PropTypes from "prop-types";
import { Box } from "@material-ui/core";

class FooterCopyright extends PureComponent {
    render() {
        const {
            alignItems,
            copyrightText,
            display,
            mt,
            order,
            width
        } = this.props;

        return (
            <Box
                alignItems={alignItems}
                display={display}
                mt={mt}
                order={order}
                width={width}
            >
                {copyrightText}
            </Box>
        );
    }
}

FooterCopyright.propTypes = {
    alignItems: PropTypes.string,
    copyrightText: PropTypes.string.isRequired,
    display: PropTypes.string,
    mt: PropTypes.shape({
        xs: PropTypes.number,
        md: PropTypes.number
    }),
    order: PropTypes.shape({
        xs: PropTypes.number,
        md: PropTypes.number
    }),
    width: PropTypes.shape({
        xs: PropTypes.string,
        md: PropTypes.string
    })
};

FooterCopyright.defaultProps = {
    alignItems: "center",
    display: "flex",
    mt: { xs: 7, md: 0 },
    order: { xs: 2, md: 1 },
    width: { xs: "100%", md: "auto" }
};

export default FooterCopyright;