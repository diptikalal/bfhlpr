import React, { Component } from "react";

class Loader extends Component {
  render() {
    return <span>Page is loading</span>;
  }
}

export default Loader;
