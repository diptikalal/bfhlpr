import React, { Component, Fragment } from "react";

import Header from "../../Modules/Header/Header.container";
import Footer from "../../Modules/Footer/Footer.component";

function withLayout(WrappedComponent) {
  return class extends Component {
    render() {
      return (
        <Fragment>
          <Header />
          <WrappedComponent {...this.props} />
          <Footer />
        </Fragment>
      );
    }
  }
};

export default withLayout;
