import React, { Component } from 'react';
import { Box, Link, Typography } from '@material-ui/core';
class HeaderMenu extends Component {
  render() {
    return (
      <Box component="ul" flexGrow={1} display="flex">
        <Box component="li" fontWeight={700} className="size-xxii-xviii" color="shade100.main" mr={{ md: 4, lg: 10 }}>
          <Link href="" color="inherit">
            Managed Care
            <Typography className="size-xiv medium">More than just insurance</Typography>
          </Link>
        </Box>
        <Box component="li" fontWeight={700} className="size-xxii-xviii" color="shade100.main" mr={{ md: 4, lg: 10 }}>
          <Link href="" color="inherit">
            HealthRx
            <Typography className="size-xiv medium">Digital manager</Typography>
          </Link>
        </Box>
        <Box component="li" fontWeight={700} className="size-xxii-xviii" color="shade100.main" mr={{ md: 4, lg: 10 }}>
          <Link href="" color="inherit">
            Health Card
            <Typography className="size-xiv medium">Hospital membership</Typography>
          </Link>
        </Box>
        <Box component="li" fontWeight={700} className="size-xxii-xviii" color="shade100.main" mr={{ md: 4, lg: 10 }}>
          <Link href="" color="inherit">
            DoctorRx
            <Typography className="size-xiv medium">Solution for doctors</Typography>
          </Link>
        </Box>
        <Box component="li" fontWeight={700} className="size-xxii-xviii" color="shade100.main" mr={{ md: 4, lg: 10 }}>
          <Link href="" color="inherit">
            Customer Service
            <Typography className="size-xiv medium">FAQs, feedback and more</Typography>
          </Link>
        </Box>
      </Box>
    )
  }
}

export default HeaderMenu;