import React, { PureComponent } from 'react';
import { CardContent, Box } from '@material-ui/core';
class PreferredChoiceCard extends PureComponent {
  render() {
    const { choiceTitle, choiceSubtitle, titleColor } = this.props;
    return (
      <React.Fragment key={'homepage-preferred-choice-card'}>
        <CardContent className="card-title">
          <Box component="h6" className="size-xxxii-xx" color={titleColor}>
            {choiceTitle}
          </Box>
          <Box component="label" className="size-xxiv-xii" display="flex" pt={{ x: 0, md: 4 }} color="shade20.main">
            {choiceSubtitle}
          </Box>
        </CardContent>
      </React.Fragment>
    );
  }
}

export default PreferredChoiceCard;