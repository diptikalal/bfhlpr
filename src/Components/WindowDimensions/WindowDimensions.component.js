import React, { PureComponent } from "react";

import {
  addNewEventListener,
  removeExistingEventListener
} from "../../Common/CommonScript";
import {
  REACT_APP_EXTRA_LARGE_DEVICE_BREAKPOINT,
  REACT_APP_EXTRA_SMALL_DEVICE_BREAKPOINT,
  REACT_APP_LARGE_DEVICE_BREAKPOINT,
  REACT_APP_MEDIUM_DEVICE_BREAKPOINT,
  REACT_APP_SMALL_DEVICE_BREAKPOINT
} from "./WindowDimensions-constants/WindowDimensions.constants";

const withWindowDimensions = WrappedComponent => {
  return class extends PureComponent {
    state = {
      height: 0,
      isXS: false,
      isSM: false,
      isMD: false,
      isLG: false,
      isXL: false,
      width: 0
    };

    componentDidMount() {
      this.updateWindowDimensions();
      addNewEventListener(window, "resize", this.updateWindowDimensions);
    }

    componentWillUnmount() {
      removeExistingEventListener(window, "resize", this.updateWindowDimensions);
    }

    updateWindowDimensions = () => {
      const {
        innerHeight: height,
        innerWidth: width
      } = window;
      this.setState({
        height,
        width,
        isXS: width > REACT_APP_EXTRA_SMALL_DEVICE_BREAKPOINT,
        isSM: width >= REACT_APP_SMALL_DEVICE_BREAKPOINT,
        isMD: width >= REACT_APP_MEDIUM_DEVICE_BREAKPOINT,
        isLG: width >= REACT_APP_LARGE_DEVICE_BREAKPOINT,
        isXL: width >= REACT_APP_EXTRA_LARGE_DEVICE_BREAKPOINT
      });
    };

    render() {
      const {
        height,
        isXS,
        isSM,
        isMD,
        isLG,
        isXL,
        width
      } = this.state;
      return (
        <WrappedComponent
          {...this.props}
          windowWidth={width}
          windowHeight={height}
          windowIsXS={isXS}
          windowIsSM={isSM}
          windowIsMD={isMD}
          windowIsLG={isLG}
          windowIsXL={isXL}
        />
      );
    }
  };
}

export default withWindowDimensions;