import React, { PureComponent } from "react";

import { 
    addNewEventListener,
    removeExistingEventListener
} from "../../Common/CommonScript";

function withWindowScroll(WrappedComponent) {
    return class extends PureComponent {
        state = { 
            hasScrolledDown: false,
            scrollPositionY: 0
         };

        componentDidMount() {
            addNewEventListener(window, "scroll", this.updateWindowScroll);
        }

        componentWillUnmount() {
            removeExistingEventListener(window, "scroll", this.updateWindowScroll);
        }

        updateWindowScroll = () => {
            this.setState(({ scrollPositionY: prevScrollPositionY }) => ({
                hasScrolledDown: window.scrollY > prevScrollPositionY,
                scrollPositionY: window.scrollY
            }));
        };

        render() {
            const { 
                hasScrolledDown,
                scrollPositionY
             } = this.state;
            return (
                <WrappedComponent
                    {...this.props}
                    hasScrolledDown={hasScrolledDown}
                    scrollPositionY={scrollPositionY}
                />
            );
        }
    };
}

export default withWindowScroll;