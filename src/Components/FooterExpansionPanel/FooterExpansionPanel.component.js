import React, { PureComponent } from "react";

import PropTypes from "prop-types";
import {
    Box,
    ExpansionPanel,
    ExpansionPanelSummary,
    ExpansionPanelDetails,
    Link
} from '@material-ui/core';
import { ExpandMore } from '@material-ui/icons';

class FooterExpansionPanel extends PureComponent {
    render() {
        const {
            alignItems,
            ariaControls,
            bgColor,
            className,
            color,
            component,
            details,
            disabled,
            flexDirection,
            fontSize,
            fontWeight,
            id,
            mb,
            mt,
            px,
            py,
            style,
            summary,
            width
        } = this.props;

        return (
            <ExpansionPanel
                defaultExpanded
                className={className[0]}
                disabled={disabled}
            >
                <Box
                    color={color[0]}
                    bgcolor={bgColor}
                    mt={mt}
                >
                    <ExpansionPanelSummary
                        expandIcon={
                            <ExpandMore
                                style={style}
                            />
                        }
                        aria-controls={ariaControls}
                        id={id}
                        className={className[1]}
                    >
                        <Box
                            color={color[1]}
                            component={component[0]}
                            fontSize={fontSize[0]}
                            fontWeight={fontWeight[0]}
                            px={px[0]}
                            py={py[0]}
                            width={width}
                        >
                            {summary}
                        </Box>
                    </ExpansionPanelSummary>
                </Box>
                <ExpansionPanelDetails
                    className={className[2]}
                >
                    <Box
                        component={component[1]}
                        flexDirection={flexDirection}
                        px={px[1]}
                        py={py[1]}
                    >
                        {
                            details.map(({
                                href,
                                label
                            }, index) => (
                                    <Box
                                        key={`footer-link-${index}`}
                                        alignItems={alignItems}
                                        color={color[2]}
                                        component={component[2]}
                                        fontSize={fontSize[1]}
                                        fontWeight={fontWeight[1]}
                                        mb={mb}
                                    >
                                        <Link
                                            href={href}
                                            color={color[3]}
                                        >
                                            {label}
                                        </Link>
                                    </Box>
                                ))
                        }
                    </Box>
                </ExpansionPanelDetails>
            </ExpansionPanel>
        );
    }
}

FooterExpansionPanel.propTypes = {
    alignItems: PropTypes.string,
    ariaControls: PropTypes.string.isRequired,
    bgColor: PropTypes.shape({
        xs: PropTypes.string,
        md: PropTypes.string
    }),
    className: PropTypes.arrayOf(PropTypes.string),
    color: PropTypes.arrayOf(PropTypes.string),
    component: PropTypes.arrayOf(PropTypes.string),
    details: PropTypes.arrayOf(
        PropTypes.shape({
            href: PropTypes.string,
            label: PropTypes.string
        })
    ).isRequired,
    disabled: PropTypes.bool.isRequired,
    flexDirection: PropTypes.string,
    fontSize: PropTypes.arrayOf(PropTypes.string),
    fontWeight: PropTypes.arrayOf(PropTypes.number),
    id: PropTypes.string.isRequired,
    mt: PropTypes.shape({
        xs: PropTypes.number,
        md: PropTypes.number
    }).isRequired,
    px: PropTypes.arrayOf(
        PropTypes.shape({
            xs: PropTypes.number,
            md: PropTypes.number
        })
    ),
    py: PropTypes.arrayOf(
        PropTypes.shape({
            xs: PropTypes.number,
            md: PropTypes.number
        })
    ),
    style: PropTypes.shape({
        fontSize: PropTypes.number,
        color: PropTypes.string
    }),
    summary: PropTypes.string.isRequired,
    width: PropTypes.string
};

FooterExpansionPanel.defaultProps = {
    alignItems: "center",
    bgColor: { xs: "denary.main", md: "transparent" },
    className: ["footer-expansionPanel", "p-0", "px-0"],
    color: ["primary.main", "secondary.main", "secondary.contrastText", "inherit"],
    component: ["h5", "ul", "li"],
    flexDirection: "column",
    fontSize: ["2rem", "1.6rem"],
    fontWeight: [700, 500],
    mb: 4,
    px: [{ xs: 4, md: 0 }, { xs: 4, md: 0 }],
    py: [{ xs: 4, md: 0 }, { xs: 4, md: 0 }],
    style: { fontSize: 34, color: "#fff" },
    width: "100%"
};

export default FooterExpansionPanel;