import React, { Component } from "react";
import { className } from "postcss-selector-parser";

class ImageComponent extends Component {
  render() {
    const {
      src,
      alt,
      className,
    } = this.props;

    return (
      <img width="100%" src={src} alt={alt} className={className} />
    );
  }
}

export default ImageComponent;
