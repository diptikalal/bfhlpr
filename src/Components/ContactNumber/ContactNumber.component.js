import React, { PureComponent } from "react";

import PropTypes from "prop-types";
import { Box } from '@material-ui/core';
import { CallOutlined } from '@material-ui/icons';

class ContactNumber extends PureComponent {
    render() {
        const {
            alignItems,
            color,
            component,
            contactNumber,
            display,
            fontSize,
            pl,
            pt,
            style
        } = this.props;

        return (
            <Box
                alignItems={alignItems}
                color={color[0]}
                display={display}
                fontSize={fontSize}
                pl={pl[0]}
                pt={pt}
            >
                <CallOutlined
                    color={color[1]}
                    style={style}
                />
                <Box
                    component={component}
                    pl={pl[1]}
                >
                    {contactNumber}
                </Box>
            </Box>
        );
    }
}

ContactNumber.propTypes = {
    alignItems: PropTypes.string,
    color: PropTypes.arrayOf(PropTypes.string),
    contactNumber: PropTypes.string.isRequired,
    component: PropTypes.string,
    display: PropTypes.string,
    fontSize: PropTypes.string,
    pl: PropTypes.arrayOf(
        PropTypes.oneOfType([
            PropTypes.shape({
                xs: PropTypes.number,
                md: PropTypes.number
            }),
            PropTypes.number
        ])
    ),
    pt: PropTypes.shape({
        xs: PropTypes.number,
        md: PropTypes.number
    }),
    style: PropTypes.shape({
        fontSize: PropTypes.number
    })
};

ContactNumber.defaultProps = {
    alignItems: "center",
    color: ["shade0.main", "secondary"],
    component: "span",
    display: "flex",
    fontSize: "2rem",
    pl: [{ xs: 0, md: 11 }, 3],
    pt: { xs: 5, md: 0 },
    style: { fontSize: 34 }
};

export default ContactNumber;