import React, { Component } from "react";
import { Link } from "react-router-dom";

class LinkComponent extends Component {
  render() {
    const { Routes } = this.props;
    return (
      <Link key={Routes.path} to={Routes.path}>
        {Routes.name}
      </Link>
    );
  }
}

export default LinkComponent;
