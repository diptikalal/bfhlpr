import React, { Component, Fragment } from "react";
import PropTypes from "prop-types";
import Slider from "react-slick";

import { Typography, Box } from "@material-ui/core";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import withWindowDimensions from "../WindowDimensions/WindowDimensions.component";
import Image from "../Image/image.component";
import { bannerSettings, reviewSliderSettings } from "./Carousel.settings";
import ReviewCard from "../../Modules/Reviews/Reviews.card.component";
import DownloadButton from "../DownloadButton/DownloadButton.component";

class Carousel extends Component {
  render() {
    const {
      cardSlider,
      showDownloadButton,
      sliderData,
      windowIsMD
    } = this.props;
    const sliderSettings = cardSlider ? reviewSliderSettings : bannerSettings;

    return (
      <Fragment key="carousel">
        <Slider {...sliderSettings} className="default-single-slider">
          {
            cardSlider
              ? sliderData.map((slider, index) => (
                <ReviewCard
                  key={`slider-review-card-${index}`}
                  cardContents={slider}
                />
              ))
              : sliderData.map(({
                alt,
                figcaption,
                figcaption1,
                figcaption2,
                figcaption3,
                imagesrc_lg,
                imagesrc_sm,
                subTitle
              }, index) => (
                  <Box key={`slider-box-${index}`} height="100vh" component="section" className="home-slider">
                    <Image
                      src={windowIsMD ? imagesrc_lg : imagesrc_sm}
                      alt={alt}
                      className="image-cover"
                    />

                    <Box
                      component="figcaption"
                      position="absolute"
                      width="100%" height="100vh"
                      px={{ xs: "21%", md: "8%", lg: "8%" }}
                      top={{ xs: "0" }} bottom={{ xs: "0" }}
                      display="flex" flexDirection="column" justifyContent="center" alignItems={{ xs: "center", md: "start" }}
                      textAlign={{ xs: "center", md: "left" }}
                    >
                      {
                        index === 0
                          ? (
                            <Box component="h1" className="size-liii-xxx" color="shade60.main" mb={{ xs: 4, lg: 7 }}>
                              {figcaption}
                              <Box component="span" display={{ xs: "inline-block", md: "flex" }} color="shade60.main">
                                {figcaption1}
                                <Box component="span" display={{ xs: "inline-block" }} color="primary.main">
                                  &nbsp;{figcaption2}&nbsp;
                                </Box>
                                {figcaption3}
                              </Box>
                            </Box>
                          )
                          : (
                            <Box component="h2" className="size-liii-xxx" color="shade60.main" mb={{ xs: 4, lg: 7 }}>
                              {figcaption}
                              <Box component="span" display="flex" color="shade60.main">
                                {figcaption1}
                                <Box component="span" display="inline-block" color="primary.main">
                                  &nbsp;{figcaption2}&nbsp;
                              </Box>
                                {figcaption3}
                              </Box>
                            </Box>
                          )
                      }
                      {/* <Typography variant="h6">{subTitle}</Typography> */}

                      {showDownloadButton && <DownloadButton />}
                    </Box>
                  </Box>
                )
              )
          }
        </Slider>
      </Fragment>
    );
  }
}

Carousel.propTypes = {
  sliderData: PropTypes.arrayOf(PropTypes.object).isRequired,
  showDownloadButton: PropTypes.bool
};

Carousel.defaultProps = {
  showDownloadButton: false
};

export default withWindowDimensions(Carousel);
