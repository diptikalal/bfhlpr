import React, { PureComponent } from "react";

import PropTypes from "prop-types";
import {
    ExpansionPanel,
    ExpansionPanelSummary,
    ExpansionPanelDetails,
    Typography
} from '@material-ui/core';

class FooterInfoSection extends PureComponent {
    render() {
        const {
            ariaControls,
            heading,
            id,
            value
        } = this.props;

        return (
            <ExpansionPanel
                className="no-background no-shadow"
                defaultExpanded
            >
                <ExpansionPanelSummary
                    aria-controls={ariaControls}
                    className="px-0"
                    id={id}
                >
                    <Typography
                        className="px-xs-4"
                        color="textPrimary"
                        component="h5"
                        fontWeight="bold"
                        variant="caption"
                    >
                        {heading}
                    </Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails
                    className="px-0"
                >
                    <Typography
                        className="size-xvi px-xs-4"
                        color="textPrimary"
                        component="p"
                        fontWeight="500"
                    >
                        {value}
                    </Typography>
                </ExpansionPanelDetails>
            </ExpansionPanel>
        );
    }
}

FooterInfoSection.propTypes = {
    ariaControls: PropTypes.string.isRequired,
    heading: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired
};

export default FooterInfoSection;