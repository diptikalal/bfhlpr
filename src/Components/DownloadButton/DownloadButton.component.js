import React, { PureComponent } from "react";
import PropTypes from "prop-types";

import { Box, Button } from '@material-ui/core';

class DownloadButton extends PureComponent {
  render() {
    const {
      color,
      component,
      fontSize,
      fontWeight,
      label,
      px,
      py,
      size,
      variant,
      width
    } = this.props;

    return (
      <Button
        color={color}
        size={size}
        variant={variant}
      >
        <Box
          component={component}
          width={width}
          fontSize={fontSize}
          fontWeight={fontWeight}
          px={px}
          py={py}
        >
          {label}
        </Box>
      </Button>
    );
  }
}

DownloadButton.propTypes = {
  color: PropTypes.string,
  component: PropTypes.string,
  fontSize: PropTypes.string,
  fontWeight: PropTypes.number,
  label: PropTypes.string,
  px: PropTypes.shape({
    xs: PropTypes.number,
    md: PropTypes.number,
    lg: PropTypes.number
  }),
  py: PropTypes.shape({
    xs: PropTypes.number,
    md: PropTypes.number,
    lg: PropTypes.number
  }),
  size: PropTypes.string,
  variant: PropTypes.string,
  width: PropTypes.shape({
    xs: PropTypes.string
  })
};

DownloadButton.defaultProps = {
  color: "primary",
  component: "span",
  fontSize: "1.6rem",
  fontWeight: 500,
  label: "Download the app",
  px: { xs: 1, md: 0, lg: 1 },
  py: { xs: 1, md: 0, lg: 1 },
  size: "small",
  variant: "contained",
  width: { xs: "177px" }
};

export default DownloadButton;
