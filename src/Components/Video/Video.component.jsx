import React, { Component, createRef } from "react";
import PropTypes from "prop-types";

import { Box, Card, CardMedia, IconButton } from "@material-ui/core";
import { PlayCircleOutline } from "@material-ui/icons";

class Video extends Component {
  videoRef = createRef();

  handleClick = () => {
    this.videoRef.current.play();
  };

  render() {
    const {
      alt,
      height,
      src,
      width
    } = this.props;

    return (
      <Box component="div" margin="0 auto">
        <Card>
          <CardMedia
            component="video"
            height={height}
            ref={this.videoRef}
            src={src}
            title={alt}
            width={width}
          >
            Your browser doesn't support HTML5 video tag.
          </CardMedia>
          <IconButton aria-label="play/pause">
            <PlayCircleOutline onClick={this.handleClick} />
          </IconButton>
        </Card>
      </Box>
    );
  }
}

Video.propTypes = {
  alt: PropTypes.string.isRequired,
  height: PropTypes.number.isRequired,
  src: PropTypes.string.isRequired,
  width: PropTypes.number.isRequired
};

export default Video;
