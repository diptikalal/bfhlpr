import React, { PureComponent } from "react";

import PropTypes from "prop-types";
import { Link } from '@material-ui/core';

class FooterSocialMediaLink extends PureComponent {
    render() {
        const {
            className,
            children,
            color,
            href
        } = this.props;

        return (
            <Link
                color={color}
                className={className}
                href={href}
            >
                {children}
            </Link>
        );
    }
}

FooterSocialMediaLink.propTypes = {
    className: PropTypes.string,
    children: PropTypes.node.isRequired,
    color: PropTypes.string,
    href: PropTypes.string.isRequired
};

FooterSocialMediaLink.defaultProps = {
    className: "footer-icon",
    color: "inherit"
};

export default FooterSocialMediaLink;