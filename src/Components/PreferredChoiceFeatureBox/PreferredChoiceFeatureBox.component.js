import React, { PureComponent } from 'react';
import { Box, Grid, Typography } from '@material-ui/core';
import { VideoCall } from '@material-ui/icons';

class PreferredChoiceFeatureBox extends PureComponent {
  render() {
    const { className, featureTitle } = this.props;
    return (
      <React.Fragment key={'homepage-preferred-choice-feature-box'}>
        <Grid component="li" item xs={6} md={3} spacing={5}>
          <Box width="100%" height={{ xs: "13rem", md: "inherit" }} py={{ xs: 5, md: 12 }} mb={{ xs: 6, md: 0 }} justifyContent="center" textAlign="center" className={className}>
            <Box component="p" margin="0 -1.6rem" padding="0 2.4rem" display="flex" justifyContent="center" bgcolor="shade0.main" flexDirection="column">
              <Typography component="span"><VideoCall className="size-lxxii-lxii" /></Typography>
              <Box component="span" fontSize={{ xs: '1.3rem', md: '2rem' }} fontWeight="bold">{featureTitle}</Box>
            </Box>
          </Box>
        </Grid>
      </React.Fragment>
    );
  }
}

export default PreferredChoiceFeatureBox;