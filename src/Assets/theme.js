
import { createMuiTheme } from '../../node_modules/@material-ui/core/styles';
import red from '../.././node_modules/@material-ui/core/colors/red';


const theme = createMuiTheme({
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 960,
      lg: 1280,
      xl: 1920,
    },
    keys: ['xs', 'sm', 'md', 'lg', 'xl'],
    up: function (key) { return `@media (min-width:${this.values[key]}px)`; },
  },

  palette: {
    primary: {
      main: '#5c2d90',
      contrastText: '#fff',
    },

    secondary: {
      main: '#fdca7b',
      contrastText: '#fff',
    },

    tertiary: {
      main: '#50bfa8',
      contrastText: '#ffffff',
    },

    quaternary: {
      main: '#f2776b',
    },

    quinary: {
      main: '#f2b352',
    },

    senary: {
      main: '#f2b352', // not in used
    },

    septenary: {
      main: '#eae554', //not in used
    },

    octonary: {
      main: '#cce9f4', //not in used
    },

    nonary: {
      main: '#53a70f', //not in used
    },

    denary: {
      main: '#481b7b',
    },

    donary: {
      main: '#d3dff2',
    },

    shade100: {
      main: '#000',
    },

    shade60: {
      main: '#4b4b4b',
    },

    shade30: {
      main: '#707070',
    },

    shade20: {
      main: '#8a8a8a',
    },

    shade0: {
      main: '#fff',
    },

    text: {
      primary: '#fff',
      secondary: '#fdca7b',
      tertiary: '#50bfa8',
      quaternary: '#f2776b',
      quinary: '#f2b352',
      shade100: '#000',
      shade70: '#333',
      shade60: '#4b4b4b',
      shade30: '#707070',
      shade20: '#8a8a8a',
      shade0: '#fff',
    },

    error: {
      main: red.A400,
    },

    background: {
      default: '#fff',
    },
  },
  typography: {
    fontFamily: [
      '"Maven Pro"',
      'sans-serif',
    ].join(','),

    typography: {
      htmlFontSize: '1rem',
    },

    props: {
      MuiTypography: {
        variantMapping: {
          body3: 'h4',
        },
      },
    },

    useNextVariants: true,

    h2: {
      fontSize: '5.6rem',
    },

    h4: {
      fontSize: '3.6rem',
    },

    h6: {
      fontSize: '3.2rem',
    },

    subtitle1: {
      fontSize: '2.4rem',
    },

    caption: {
      fontSize: '2rem',
    },
    // subtitle6: {
    //   fontSize: 16,
    // },
    // body1: {
    //   fontWeight: 500,
    // },
  },

  spacing: factor => `${0.4 * factor}rem`,
});

export default theme;