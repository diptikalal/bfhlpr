const TAB_CONTENTS = [
  {
    title: [
      'We come from the',
      ' house of Bajaj Finserv Limited.'
    ],
    images: [0],
    buttonLabel: 'GET TO KNOW US BETTER'
  },
  {
    title: [
      `If you are passionate about radically transforming the healthcare ecosystem 
            with financial and technological integration, then HealthRx is the right place for you!`
    ],
    images: [{
      URL: require('../../../Assets/Images/static/ideate.png'),
      label: 'Ideate',
    },
    {
      URL: require('../../../Assets/Images/static/co-create.png'),
      label: 'Co-create',
    },
    {
      URL: require('../../../Assets/Images/static/impact.png'),
      label: 'Impact',
    }],
    buttonLabel: 'JOIN THE TEAM',
  }
];
export {
    TAB_CONTENTS
};