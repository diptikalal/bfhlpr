import React, { Fragment, useCallback, useRef, useState } from "react";
import { Box, Button, Grid, Paper, Tab, Tabs } from "@material-ui/core";

import LinkTab from "../../Components/LinkTab/LinkTab.component";
import TabPanel from "../../Components/TabPanel/TabPanel.component";
import { TAB_CONTENTS } from "./WhoAreWeWorkWithUs-constants/WhoAreWeWorkWithUs.constants";
import '../../Assets/Styles/pages/home-page.scss';

const customProps = index => ({
  id: `nav-tab-${index}`,
  "aria-controls": `nav-tabpanel-${index}`,
});


const Content = ({
  buttonLabel = '',
  href = '',
  images = [],
  title = ''
}) => (
    <Fragment>
      <Box component="p" maxWidth={{ xs: '100%', md: '128rem' }} mt={{ xs: 0, md: 7 }} mb={{ xs: 0, md: 22 }} className="size-xxviii-xvi medium" color="text.shade70" textAlign="center">{title}</Box>
      {
        images.length > 0
        && (
          <Grid container spacing={10}>
            {
              images.map((imageId, index) => (
                <Grid item key={`grid-image-${index}`} component="figure" xs="12" md="4" spacing={10} className="relative">
                  <img width="100%" src={images[index].URL} alt={`Alternate text for ${imageId}`} />
                  <Box component="figcaption" className="size-xxxii-xx bold background-bottom-overlay" position="absolute"
                    bottom={{ xs: 0, md: '0' }} pb={{ xs: 0, md: '6.5rem' }} width="calc(100% - 4rem)" textAlign="center"
                    left="0" borderRadius={{ xs: 0, md: '1rem' }} mx={{ xs: 0, md: 5 }} color="text.shade0">{images[index].label}</Box>
                </Grid>
              ))
            }
          </Grid>
        )
      }
      <Box component="p" textAlign="center" mt={{ xs: 0, md: 10 }} mb={{ xs: 0, md: 32 }}>
        <Button size="large" color="primary" variant="contained" className="default-btn size-xx">{buttonLabel}</Button>
      </Box>
    </Fragment>
  );

const WhoAreWeWorkWithUs = () => {
  // const classes = useStyles();
  const [value, setValue] = useState(0);
  const contentsRef = useRef(TAB_CONTENTS.length);

  const handleChange = useCallback((event, newValue) => {
    setValue(newValue);
  }, []);

  return (
    <React.Fragment>
      <Box component="section" mt={{ xs: 0, md: 19 }} >
        <Paper className="default-paper-tab">
          <Tabs
            value={value}
            onChange={handleChange}
            aria-label="nav tabs"
            centered
            textColor="primary"
            className="default-tabs"
          >
            <Tab label="Who are we?" className="default-tab-button size-xxiv-xiv medium" aria-label="phone" {...customProps(0)} />
            <Tab label="Join us" className="default-tab-button size-xxiv-xiv medium" aria-label="phone"  {...customProps(1)} />
          </Tabs>
        </Paper>
        {
          contentsRef.current > 0
          && TAB_CONTENTS.map((_, index) => (
            <TabPanel
              key={`tab-panel-${index}`}
              value={value}
              index={index}
            >
              {index === 0 ?
                <Box>
                  <Box className="who-are-we" width="100%" justifyContent="center">
                    <Grid container component="ul" className="brand-container">
                      <Grid component="li" item xs={12} className="mr-10">
                        <Box component="figure" className="brand-block right-circle" bgcolor="shade0.main">
                          <img width="100%" src={require("../../Assets/Images/static/bfl.png")} alt="Bajaj Finserv" />
                        </Box>
                      </Grid>
                      <Grid component="li" item xs={12}>
                        <Box component="figure" className="brand-block right-circle" bgcolor="shade0.main">
                          <img width="100%" src={require("../../Assets/Images/static/bajaj-allianz.png")} alt="Bajaj Allianz" />
                        </Box>
                      </Grid>

                      <Grid component="li" item md={3} xs={12} className="who-are-center">
                        <Box component="figure" className="center-circle bottom-circle" mx={{ xs: 8, md: 10 }}>
                          <img width="100%" src={require("../../Assets/Images/static/who-are-we-circle.png")} alt="Bajaj Finserv" />
                        </Box>
                      </Grid>

                      <Grid component="li" item xs={12} className="mr-10">
                        <Box component="figure" className="brand-block left-circle" bgcolor="shade0.main">
                          <img width="100%" src={require("../../Assets/Images/static/bmarkets.png")} alt="BMarkets" />
                        </Box>
                      </Grid>
                      <Grid component="li" item xs={12}>
                        <Box component="figure" className="brand-block left-circle" bgcolor="shade0.main">
                          <img width="100%" src={require("../../Assets/Images/static/caring-baja-allianz.png")} alt="Bajaj Allianz" />
                        </Box>
                      </Grid>
                    </Grid>
                  </Box>
                  <Box component="p" display="flex" justifyContent="center">
                    <Box component="figure" maxWidth={{ xs: "100%", md: "15.8rem" }}>
                      <img width="100%" src={require("../../Assets/Images/static/healthrx.png")} alt="healthrx.png" />
                    </Box>
                  </Box>
                  <Box mt={{ xs: 0, md: 17 }} display="flex" justifyContent="center">
                    <Button color="primary" variant="contained" className="size-xx-xiv flex-grow-sm">GET TO KNOW US BETTER</Button>
                  </Box>
                </Box> :
                <Box maxWidth={{ xs: '100%', md: '128rem' }} margin="0 auto">
                  <Content {...TAB_CONTENTS[value]} />
                </Box>
              }
            </TabPanel>
          ))
        }
      </Box>
    </React.Fragment>
  );
}

export default WhoAreWeWorkWithUs;
