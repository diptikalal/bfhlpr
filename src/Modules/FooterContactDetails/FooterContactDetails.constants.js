const CONTACT_NUMBER = "020-48546000";
const EMAIL_ADDRESS = "customercare@healthrx.co.in";

export {
    CONTACT_NUMBER,
    EMAIL_ADDRESS
};