import React, { PureComponent } from "react";

import PropTypes from "prop-types";
import { Box, Grid } from "@material-ui/core";

import ContactNumber from "../../Components/ContactNumber/ContactNumber.component";
import EmailAddress from "../../Components/EmailAddress/EmailAddress.component";

import {
    CONTACT_NUMBER,
    EMAIL_ADDRESS
} from "./FooterContactDetails.constants";

class FooterContactDetails extends PureComponent {
    render() {
        const {
            alignItems,
            color,
            display,
            flexWrap,
            justifyContent,
            md,
            ml,
            py,
            pb,
            xs
        } = this.props;

        return (
            <Box
                alignItems={alignItems}
                color={color}
                display={display}
                flexWrap={flexWrap}
                justifyContent={justifyContent}
                ml={ml}
                py={py}
                pb={pb}
            >
                <Grid
                    item
                    xs={xs}
                    md={md}
                >
                    <EmailAddress emailAddress={EMAIL_ADDRESS} />
                </Grid>
                <Grid
                    item
                    xs={xs}
                    md={md}
                >
                    <ContactNumber contactNumber={CONTACT_NUMBER} />
                </Grid>
            </Box>
        );
    }
}

FooterContactDetails.propTypes = {
    alignItems: PropTypes.string,
    color: PropTypes.string,
    display: PropTypes.string,
    flexWrap: PropTypes.string,
    justifyContent: PropTypes.string,
    md: PropTypes.string,
    ml: PropTypes.shape({
        xs: PropTypes.number,
        md: PropTypes.number
    }),
    py: PropTypes.shape({
        xs: PropTypes.number,
        md: PropTypes.number
    }),
    pb: PropTypes.shape({
        xs: PropTypes.number,
        md: PropTypes.number
    }),
    xs: PropTypes.number
};

FooterContactDetails.defaultProps = {
    alignItems: "center",
    color: "shade0.main",
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "flex-end",
    md: "auto",
    ml: { xs: 4, md: 0 },
    py: { xs: 0, md: 11 },
    pb: { xs: 11, md: 11 },
    xs: 12
};

export default FooterContactDetails;