import React, { Fragment, PureComponent } from 'react';
import PropTypes from 'prop-types';

import { Box } from '@material-ui/core';

class Legacy extends PureComponent {
  render() {
    const { description, subTitle, title } = this.props;
    return (
      <Fragment key='legacy'>
        <Box component='h2'>{title}</Box>
        <Box component='h3'>{subTitle}</Box>
        <Box component='h4'>{description}</Box>
        <hr />
      </Fragment>
    );
  }
}

Legacy.propTypes = {
  description: PropTypes.string.isRequired,
  subTitle: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
};

export default Legacy;
