import React, { PureComponent } from "react";

import PropTypes from "prop-types";
import { Box } from "@material-ui/core";
import {
    Facebook,
    Instagram,
    LinkedIn,
    Twitter
} from "@material-ui/icons";

import FooterSocialMediaLink from "../../Components/FooterSocialMediaLink/FooterSocialMediaLink.component";

const FOOTER_SOCIAL_MEDIA_ITEMS_CONTENT = [
    {
        childComponent: <LinkedIn />,
        href: "mailto:customercare@healthrx.co.in"
    },
    {
        childComponent: <Facebook />,
        href: "mailto:customercare@healthrx.co.in"
    },
    {
        childComponent: <Instagram />,
        href: "mailto:customercare@healthrx.co.in"
    },
    {
        childComponent: <Twitter />,
        href: "mailto:customercare@healthrx.co.in"
    }
];

class FooterSocialMediaItems extends PureComponent {
    render() {
        const {
            order,
            width
        } = this.props;

        return (
            <Box
                order={order}
                width={width}
            >
                {
                    FOOTER_SOCIAL_MEDIA_ITEMS_CONTENT.map(({
                        childComponent,
                        href
                    }, index) => (
                            <FooterSocialMediaLink
                                key={`social-media-link-${index}`}
                                href={href}
                            >
                                {childComponent}
                            </FooterSocialMediaLink>
                        ))
                }
            </Box>
        );
    }
}

FooterSocialMediaItems.propTypes = {
    order: PropTypes.shape({
        xs: PropTypes.number,
        md: PropTypes.number
    }),
    width: PropTypes.shape({
        xs: PropTypes.string,
        md: PropTypes.string
    })
};

FooterSocialMediaItems.defaultProps = {
    order: { xs: 1, md: 2 },
    width: { xs: "100%", md: "auto" }
};

export default FooterSocialMediaItems;