
const choiceDetails = [
  {
    id: 0,
    title: 'Get Virtual Consultation',
    titleColor: 'text.quinary',
    subTitle: 'from the comfort of your home',
    className: 'virtual-consulation',
    attributes: {
      display: 'flex', width: { xs: '100%', md: '33%' },
    },
    features: {
      featureSectionTitle: 'Fast. Easy. Secure.',
      featureList: [
        {
          featureTitle: 'Call, video and chat',
          className: 'card-details-virtual'
        },
        {
          featureTitle: 'Specialist doctors',
          className: 'card-details-virtual'
        },
        {
          featureTitle: 'Quick Prescription',
          className: 'card-details-virtual'
        },
        {
          featureTitle: 'Strictly confidential',
          className: 'card-details-virtual'
        }
      ],
      customButtonText: 'GET THE APP'
    }
  },
  {
    id: 1,
    title: 'Explore Healthcare Plans',
    titleColor: 'text.tertiary',
    subTitle: "which are more than just insurance",
    imgURL: require('../../Assets/Images/static/health-care.png'),
    className: 'health-plan',
    attributes: {
      display: 'flex', width: { xs: '100%', md: '33%' },
    },
    features: {
      featureSectionTitle: 'Proactive. Preventive. Personalized.',
      featureList: [
        {
          featureTitle: 'Health Check-ups',
          className: 'card-details-health'
        },
        {
          featureTitle: 'Doctor Consultation',
          className: 'card-details-health'
        },
        {
          featureTitle: 'Health Insurance',
          className: 'card-details-health'
        },
        {
          featureTitle: 'Digital Experience',
          className: 'card-details-health'
        }
      ],
      customButtonText: 'GET IN TOUCH'
    }
  },
  {
    id: 2,
    title: 'Live A Hearty Life',
    titleColor: 'text.quaternary',
    subTitle: 'with your personalized health manager',
    className: 'hearty-life',

    attributes: {
      display: 'flex', width: { xs: '100%', md: '33%' },
    },
    features: {
      featureSectionTitle: "Search. Track. Record.",
      featureList: [
        {
          featureTitle: 'Doctor Booking',
          className: 'card-details-hearty'
        },
        {
          featureTitle: 'Medicine Search',
          className: 'card-details-hearty'
        },
        {
          featureTitle: 'Lifestyle Tracker',
          className: 'card-details-hearty'
        },
        {
          featureTitle: 'Health Report',
          className: 'card-details-hearty'
        }
      ],
      customButtonText: 'GET A FREE DEMO'
    }
  }
];

const sectionConstants = {
  sectionTitle: 'Managing your health just got easier',
  sectionSubtitle: 'Say hello to our unique suite of offerings for all your healthcare needs',
  learnMoreBtnText: 'LEARN MORE'
};

export {
  choiceDetails,
  sectionConstants
}