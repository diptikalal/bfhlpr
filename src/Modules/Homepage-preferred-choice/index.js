import React, { Component } from 'react';
import { choiceDetails, sectionConstants } from './PreferredChoice.constants';
import { Box, Card, CardActionArea, Grid, Button } from '@material-ui/core';
import PreferredChoiceCard from '../../Components/PreferredChoiceCard/PreferredChoiceCard.component';
import PreferredChoiceFeatureBox from '../../Components/PreferredChoiceFeatureBox/PreferredChoiceFeatureBox.component';
import withWindowDimensions from "../../Components/WindowDimensions/WindowDimensions.component";
class PreferredChoice extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedChoiceData: choiceDetails[0].features,
      selectedChoiceIndex: 0,
    }
  }

  handleChoiceSelection = (selectedChoiceData, selectedChoiceIndex) => {
    this.setState({
      selectedChoiceData,
      selectedChoiceIndex
    });
  };

  render() {
    const {
      windowIsMD
    } = this.props;
    const { sectionTitle, sectionSubtitle, learnMoreBtnText } = sectionConstants;
    const { selectedChoiceData, selectedChoiceIndex } = this.state;
    return (
      <React.Fragment key={'homepage-preferred-choice-module'}>
        <Box component="section" bgcolor="primary.main">
          <Box maxWidth="100%" px={{ xs: "15%", md: 28, lg: 42 }} pt={{ xs: 9, md: 13 }} pb={{ xs: 25, md: 41 }}>
            <Box component="h3" textAlign="center" color="text.primary" className="size-xl-xxiv medium">{sectionTitle}</Box>
            <Box component="p" textAlign="center" color="text.primary" className="size-xxiv-xiv medium" pt={{ xs: 2, md: 3 }}>{sectionSubtitle}</Box>
          </Box>
        </Box>

        <Box component="section" width={{ xs: "calc(100% - 2.4rem)", md: "100%" }} px={{ xs: 0, md: '16.66%' }}
          mx={{ xs: 3, md: 10, lg: 'auto' }} mt={{ xs: -17, md: -25 }}
          className="expand-collapse-card">
          <Box component="ul" flexDirection={{ xs: "column", md: "row" }} mx={{ xs: 0, md: -5 }} display="flex" justifyContent="space-between">            {
            choiceDetails.map((choice, index) => (
              <Box component="li" flexDirection={{ xs: "column", md: "row" }} px={{ xs: 0, md: 5 }} maxHeight="115rem" display="flex" {...choice.attributes} key={`preferredChoice-card-${index}`} className={`card-container ${selectedChoiceIndex === index ? 'expand-collapse-active' : ''}`}>
                <Box mb={{ xs: 8, md: 0 }}>
                  <Card component="div" bgColor="primary.main" className="default-card-block">
                    <CardActionArea onClick={() => this.handleChoiceSelection(choice.features, index)} className={choice.className}>
                      <PreferredChoiceCard
                        choiceTitle={choice.title}
                        choiceSubtitle={choice.subTitle}
                        titleColor={choice.titleColor}
                      />
                    </CardActionArea>
                  </Card>
                  <div className={`arrow-down-${choice.className}`}></div>
                </Box>
                {
                  !windowIsMD && (
                    <Box className="default-card-details">
                      <Box component="h4" textAlign="center" className="size-xxxvi-xx" mb={{ xs: 10, md: 4 }}>
                        {choice.features.featureSectionTitle}
                      </Box>
                      <Box margin={{ xs: "1.6rem 2rem", md: "0" }} display="flex">
                        <Grid component="ul" container spacing={5}>
                          {
                            choice.features.featureList.map((choiceData, index) => (
                              <React.Fragment key={`preferredChoice-feature-box-${index}`}>
                                <PreferredChoiceFeatureBox {...choiceData} />
                              </React.Fragment>
                            ))
                          }
                        </Grid>
                      </Box>
                      <Box mx={{ xs: 7, sm: 12, md: 0 }} my={{ xs: 12, md: 0 }} display="flex">
                        <Button color="primary" variant="contained" className="size-xx-xiv flex-grow-sm">{learnMoreBtnText}</Button>
                        <Button color="primary" variant="contained" className="size-xx-xiv flex-grow-sm ml-md-10 ml-xs-3">{choice.features.customButtonText}</Button>
                      </Box>
                    </Box>
                  )}
              </Box>
            ))
          }
          </Box>

          {
            windowIsMD && (
              <React.Fragment>
                <Box width="100%" px={{ x: 4, md: "6%", xl: "6.6%" }} textAlign="center" margin="0 auto">
                  <Box component="h4" textAlign="center" className="size-xxxvi-xx" mt={{ xs: 0, md: 7, xl: 12 }} mb={{ xs: 0, md: 11, xl: 14 }}>{selectedChoiceData.featureSectionTitle}</Box>
                  <Box mt={{ xs: 4 }} mb={{ xs: 0, md: 8, lg: 18 }} display="flex">
                    <Grid component="ul" maxWidth="1100px" container spacing={8}>
                      {
                        selectedChoiceData.featureList.map((choiceData, index) => (
                          <React.Fragment key={`preferredChoice-feature-box-${index}`}>
                            <PreferredChoiceFeatureBox {...choiceData} />
                          </React.Fragment>
                        ))
                      }
                    </Grid>
                  </Box>

                  <Button size="large" color="primary" variant="contained" className="default-btn size-xx">{learnMoreBtnText}</Button>
                  <Button size="large" color="primary" variant="contained" className="default-btn size-xx ml-md-10 ml-xs-3">{selectedChoiceData.customButtonText}</Button>
                </Box>
              </React.Fragment>
            )
          }

        </Box>

      </React.Fragment >
    );
  }
}

export default withWindowDimensions(PreferredChoice);