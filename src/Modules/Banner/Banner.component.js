import React, { PureComponent } from "react";
import PropTypes from "prop-types";

import Carousel from "../../Components/Carousel/Carousel.component";

class Banner extends PureComponent {
  render() {
    const {
      showDownloadButton,
      sliderData
    } = this.props;

    return (
      <React.Fragment key="banner">
        <Carousel
          showDownloadButton={showDownloadButton}
          sliderData={sliderData}
        />
      </React.Fragment>
    );
  }
}

Banner.propTypes = {
  sliderData: PropTypes.arrayOf(PropTypes.object).isRequired,
  showDownloadButton: PropTypes.bool
};

Banner.defaultProps = {
  showDownloadButton: false
};

export default Banner;
