import React, { Component } from 'react';
import { Box, Grid, Typography, Link } from '@material-ui/core';

class DownloadTheApp extends Component {
  render() {
    return (
      <Box component="section" width="100%" bgcolor="secondary.main" pt={{ xs: 11, lg: 18 }}>
        <Box component="div" width="100%" px={{ xs: 4, md: "16.66%" }} margin="0 auto">
          <Grid container component="div">
            <Grid item xs={12} md={6}>
              <Box component="h3" display="flex" flexDirection="column" mt={{ xs: 0, md: 9 }} mb={{ xs: 0, md: 11 }}
                textAlign={{ xs: "center", md: "left" }} color="text.shade100" className="medium size-xl-xxiv">
                We go where you go.<br />
                With our own HealthRx app.
              </Box>
              <Box component="p" className="size-xxiv-xiv" color="text.shade60" textAlign={{ xs: "center", md: "left" }}
                mt={{ xs: 6, md: 0 }} mb={{ xs: 15, md: 0 }} fontWeight="bold">
                Download now and enjoy some awesome features!
              </Box>
              <Box display="flex" flexDirection={{ xs: "column", md: "row" }} mt={{ xs: 0, md: 30 }} mx={{ xs: "auto", md: "0" }}
                mb={{ xs: 8, md: 0 }} width={{ xs: "165px", md: "auto" }} textAlign={{ xs: "center", md: "start" }}>
                <Link href="https://play.google.com/store/apps/details?id=bajajfinserv.in.hrx.app&hl=en" className="download-button">
                  <img width="100%" src={require("../../Assets/Images/static/android-download.png")} alt="Android-Download" />
                </Link>

                <Link href="https://apps.apple.com/in/app/healthrx/id1487452389" className="download-button">
                  <img width="100%" src={require("../../Assets/Images/static/ios-download.png")} alt="iOS-Download" />
                </Link>
              </Box>
            </Grid>
            <Grid item xs={12} md={6}>
              <img width="100%" src={require("../../Assets/Images/static/app-download.png")} alt="HealthRx App" />
            </Grid>
          </Grid>
        </Box>
      </Box>
    );
  }
}

export default DownloadTheApp;