import React, { Component } from "react";
import PropTypes from "prop-types";

import { AppBar, Box, Button, Divider, Drawer, FormControl, Grid, InputLabel, Hidden, IconButton, InputBase, Link, MenuItem, Select, Toolbar } from '@material-ui/core';
import { Menu, CallOutlined, ChevronLeft, Close, EmailOutlined, Search } from '@material-ui/icons';

import HeaderMenu from '../../Components/HeaderMenu/HeaderMenu.component';
import DownloadButton from "../../Components/DownloadButton/DownloadButton.component";
import withWindowDimensions from "../../Components/WindowDimensions/WindowDimensions.component";
import withWindowScroll from "../../Components/WindowScroll/WindowScroll.component";

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = { open: false, searchToggle: false }
  }

  handleDrawerOpen = () => {
    this.setState({ open: true });
  };

  handleDrawerClose = () => {
    this.setState({ open: false });
  };

  handlerSearchToggle = () => {
    this.setState({ searchToggle: !this.state.searchToggle });
  }

  componentDidMount() {
    setTimeout(() => {
      const headerHeight = document.querySelector('.app-header').clientHeight;
      this.setState({ headerHeight });
    }, 0);
  }

  render() {
    const {
      hasScrolledDown,
      scrollPositionY
    } = this.props;
    // USE WHICHEVER YOU LIKE. DISCARD OTHERS.
    return (
      <React.Fragment>
        <AppBar position="fixed" className={`app-header ${(scrollPositionY > this.state.headerHeight) && hasScrolledDown ? "hide-header" : ""} ${(scrollPositionY > 10) && !hasScrolledDown ? "show-header" : ""}`}>
          <Toolbar className="p-0">
            <Box component="section" width="100%">
              <Box maxWidth="100%" display="flex" flexWrap="wrap" px={{ xs: 3, md: 28, lg: '8%' }} mb={{ xs: 0, md: 7 }}>
                <Box display="flex" alignItems="center" width={{ xs: "100%" }} color="text.shade100" justifyContent={{ xs: 'flex-end' }}
                  order={{ xs: "2", md: "1" }} flex={{ xs: "1 1 50%", md: "1 1 100%" }} py={{ xs: 3, md: 2 }}>
                  <FormControl>
                    <Select value="India" className="size-xviii-xv medium color-inherit">
                      <MenuItem className="size-xviii-xv medium shade100" value="India">India</MenuItem>
                      <MenuItem className="size-xviii-xv medium shade100" value="Pune">Pune</MenuItem>
                      <MenuItem className="size-xviii-xv medium shade100" value="Others">Others</MenuItem>
                    </Select>
                  </FormControl>
                  <Divider className="vertical-divider"></Divider>
                  <Box display="flex" className="size-xviii-xv medium" color="text.shade100">English</Box>
                </Box>
                <Box display="flex" width="100%" flex={{ xs: "1 1 50%", md: "1 1 100%" }} alignItems="center" order={{ xs: "1", md: "2" }}>
                  <Hidden mdUp>
                    <IconButton edge="start" aria-label="open drawer" onClick={this.handleDrawerOpen}>
                      <Menu className="size-xxv" />
                    </IconButton>
                  </Hidden>
                  <Box maxWidth={{ xs: "88px", md: "19rem", lg: "19rem" }} mr={{ x: 0, md: 16 }} mt={{ xs: 2, md: 0 }}>
                    <img alt="HealthRx" width="100%" src={require('../../Assets/Images/static/healthrx.png')} />
                  </Box>
                  <Hidden smDown>
                    <Box display="flex" position="relative" alignItems="center" width={{ xs: "0", md: "100%" }} justifyContent={{ xs: "flex-end", md: "flex-start" }} order={{ xs: "4" }} className="menu-nav">
                      <HeaderMenu />
                      <DownloadButton />
                    </Box>
                  </Hidden>
                </Box>
              </Box>
              <Divider></Divider>
            </Box>
          </Toolbar>
        </AppBar>
        <Drawer
          variant="persistent"
          anchor="left"
          open={this.state.open}>
          <div>
            <IconButton onClick={this.handleDrawerClose}>
              <ChevronLeft />
            </IconButton>
          </div>
        </Drawer>
      </React.Fragment >
    );
  }
}

Header.propTypes = {
  hasScrolled: PropTypes.bool,
  scrollPositionY: PropTypes.number,
};

export default withWindowScroll(withWindowDimensions(Header));
