import React, { Component } from "react";
import AppBar from "@material-ui/core/AppBar";
import {Box,Paper, Tab, Tabs } from "@material-ui/core";

class GotoTabs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTabIndex: 0,
    };
  }

  handleChange = (event, value) => {
    this.setState({ activeTabIndex: value });
  };

  render() {
    const { activeTabIndex } = this.state;
    const customProps = (index) => ({
      id: `nav-tab-${index}`,
      "aria-controls": `nav-tabpanel-${index}`,
    });

    return (
      <div>
        <AppBar position="static">
        <Box component="section" mt={{ xs: 0, md: 19 }} >
        <Paper className="default-paper-tab">
          <Tabs
            aria-label="nav tabs"
            centered
            textColor="primary"
            className="default-tabs"
            value={activeTabIndex}
            onChange={this.handleChange}
          >
            <Tab
              label="Our Legacy"
              className="default-tab-button size-xxiv-xiv medium"
              aria-label="phone"
              onClick={() => document.getElementById("legacy").scrollIntoView()}
              {...customProps(0)}
            />
            <Tab
              label="Our DNA"
              className="default-tab-button size-xxiv-xiv medium"
              aria-label="phone"
              href="#"
              {...customProps(1)}
            />
            <Tab
              label="Our Partners"
              className="default-tab-button size-xxiv-xiv medium"
              aria-label="phone"
              onClick={() =>
                document.getElementById("partners").scrollIntoView()
              }
              {...customProps(2)}
            />
          </Tabs>
          </Paper>
          </Box>
        </AppBar>
      </div>
    );
  }
}

export default GotoTabs;
