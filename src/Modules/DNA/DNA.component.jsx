import React, { Fragment, PureComponent } from "react";
import PropTypes from "prop-types";

import { Box, Grid, Typography } from "@material-ui/core";
import { VideoCall } from '@material-ui/icons';

import Video from "../../Components/Video/Video.component";

class DNA extends PureComponent {
  render() {
    const {
      description,
      subTitle,
      title,
      whyUsTitle1,
      whyUsTitle2,
      whyUsTitle3,
      whyUsTitle4,
      whyUsReasons
    } = this.props;

    return (
      <Fragment key="dna">
        <Box component="div" margin="0 auto" display="flex" flexDirection="column" textAlign="center">
          <Box component="h2">{title}</Box>
          <Box component="h3">{subTitle}</Box>
          <Video
            alt="Dive into out DNA"
            height={150}
            src=""
            width={150}
          />
          <Box component="h4">{description}</Box>
        </Box>
        <hr />
        <Box component="div" margin="0 auto" display="flex" flexDirection="column" textAlign="center">
          <Box component="h2">{whyUsTitle1}</Box>
          <Box component="h2">{whyUsTitle2}</Box>
          <Grid component="ul" maxWidth="1100px" container spacing={8}>
            {
              whyUsReasons.map(({
                description: whyUsDescription,
                title: whyUsTitle
              }, index) => (
                  <Grid key={`why-us-reason-${index}`} component="li" item xs={6} md={3} spacing={4}>
                    <Box width="100%" py={{ xs: 0, md: 12 }} justifyContent="center">
                      <Box component="p" margin="0 -1.6rem" padding="0 1.6rem" display="flex" justifyContent="center" bgcolor="shade0.main" flexDirection="column">
                        <Typography component="h5"><VideoCall className="size-lxxii" /></Typography>
                        <Box component="div" fontSize={{ xs: '1.4rem', md: '2rem' }} fontWeight="bold">
                          <Box component="div">{whyUsTitle}</Box>
                          <Box component="div">{whyUsDescription}</Box>
                        </Box>
                      </Box>
                    </Box>
                  </Grid>
                ))
            }
          </Grid>
        </Box>
        <hr />
        <Box component="div" margin="0 auto" display="flex" flexDirection="column" textAlign="center">
          <Box component="h2">{whyUsTitle3}</Box>
          <Box component="h2">{whyUsTitle4}</Box>
        </Box>
        <hr />
      </Fragment>
    );
  }
}

DNA.propTypes = {
  description: PropTypes.string.isRequired,
  subTitle: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  whyUsTitle1: PropTypes.string.isRequired,
  whyUsTitle2: PropTypes.string.isRequired,
  whyUsTitle3: PropTypes.string.isRequired,
  whyUsTitle4: PropTypes.string.isRequired,
  whyUsReasons: PropTypes.arrayOf(
    PropTypes.shape({
      description: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired
    })
  ).isRequired
};

export default DNA;
