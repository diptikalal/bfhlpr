const CIN_HEADING = "CIN";
const CIN_VALUE = "U65923PN2014PLC150522";
const COPYRIGHT_TEXT = "\u00a9 2020, Bajaj Finserv Health Limited. All rights reserved.";
const CORPORATE_ADDRESS_HEADING = "Corporate Address";
const CORPORATE_ADDRESS_VALUE = "Phoenix Fountainhead, 4th floor, Nagar Road, Clover Park, Viman Nagar, Pune, Maharashtra - 411014";
const FOOTER_LINKS_CONTENT = [
    [
        {
            ariaControls: "bfhl-content",
            details: [
                {
                    href: "/about-us",
                    label: "About Us"
                },
                {
                    href: "/customer-service",
                    label: "Customer Service"
                },
                {
                    href: "/work-with-us",
                    label: "Work with Us"
                },
                {
                    href: "/privacy-policy",
                    label: "Privacy Policy"
                },
                {
                    href: "/terms-and-conditions",
                    label: "Terms \u0026 Conditions"
                },
                {
                    href: "/sitemap",
                    label: "Sitemap"
                }
            ],
            id: "bfhl-header",
            mt: {
                xs: 13,
                md: 18
            },
            summary: "BFHL"
        }
    ],
    [
        {
            ariaControls: "doctors-content",
            details: [
                {
                    href: "/doctor-rx",
                    label: "DoctorRx"
                }
            ],
            id: "doctors-header",
            mt: {
                xs: 1 / 2,
                md: 18
            },
            summary: "For Doctors"
        },
        {
            ariaControls: "health-content",
            details: [
                {
                    href: "/health-rx",
                    label: "HealthRx"
                },
                {
                    href: "/managed-care",
                    label: "Managed Care"
                }
            ],
            id: "health-header",
            mt: {
                xs: 1 / 2,
                md: 0
            },
            summary: "Health Enthusiasts"
        }
    ],
    [
        {
            ariaControls: "partnership-content",
            details: [
                {
                    href: "/rhc-loyalty-card",
                    label: "RHC Loyalty Card"
                }
            ],
            id: "partnership-header",
            mt: {
                xs: 1 / 2,
                md: 18
            },
            summary: "Partnership"
        },
        {
            ariaControls: "managed-content",
            details: [
                {
                    href: "/complete-health-solution-package",
                    label: "Complete Health Solution Package"
                },
                {
                    href: "/pro-health-solution-package",
                    label: "Pro Health Solution Package"
                },
                {
                    href: "/super-health-solution-package",
                    label: "Super Health Solution Package"
                },
                {
                    href: "/hospicare-plus-package",
                    label: "Hospicare Plus Package"
                }
            ],
            id: "managed-header",
            mt: {
                xs: 1 / 2,
                md: 0
            },
            summary: "Managed Care"
        }
    ]
];
const REGISTERED_OFFICE_HEADING = "Registered Office";
const REGISTERED_OFFICE_VALUE = "Bajaj Auto Limited Complex, Mumbai – Pune Road, Akurdi, Pune – 411 035";

export {
    CIN_HEADING,
    CIN_VALUE,
    COPYRIGHT_TEXT,
    CORPORATE_ADDRESS_HEADING,
    CORPORATE_ADDRESS_VALUE,
    FOOTER_LINKS_CONTENT,
    REGISTERED_OFFICE_HEADING,
    REGISTERED_OFFICE_VALUE
};