import React, { Component } from 'react';

import PropTypes from 'prop-types';
import { Box, Divider, Grid } from '@material-ui/core';

import FooterContactDetails from '../FooterContactDetails/FooterContactDetails.component';
import FooterCopyright from '../../Components/FooterCopyright/FooterCopyright.component';
import FooterExpansionPanel from '../../Components/FooterExpansionPanel/FooterExpansionPanel.component';
import FooterInfoSection from '../../Components/FooterInfoSection/FooterInfoSection.component';
import FooterLogo from '../FooterLogo/FooterLogo.component';
import FooterSocialMediaItems from '../FooterSocialMediaItems/FooterSocialMediaItems.component';
import withWindowDimensions from '../../Components/WindowDimensions/WindowDimensions.component';
import {
  CIN_HEADING,
  CIN_VALUE,
  COPYRIGHT_TEXT,
  CORPORATE_ADDRESS_HEADING,
  CORPORATE_ADDRESS_VALUE,
  FOOTER_LINKS_CONTENT,
  REGISTERED_OFFICE_HEADING,
  REGISTERED_OFFICE_VALUE,
} from './Footer.constants';
import '../../Assets/Styles/components/expansion-panel.scss';

class Footer extends Component {
  constructor(props) {
    super(props);
    this.state = { expanded: true, setExpanded: true };
  }

  render() {
    const { windowIsMD } = this.props;

    return (
      <Box component='footer' bgcolor='primary.main' id='footer'>
        <Box maxWidth='100%' px={{ xs: 0, md: 28, lg: 42 }}>
          <Grid
            container
            component='section'
            justify={'space-between'}
            alignItems={'center'}
            className='no-margin'
          >
            <Grid item xs={12} sm={4}>
              <FooterLogo />
            </Grid>
            <Grid item xs={12} md={8}>
              <FooterContactDetails />
            </Grid>
          </Grid>
          <Box
            mx={{ xs: 0, md: -28, lg: -42 }}
            maxWidth={{ xs: '100%', md: 'none' }}
          >
            <Grid item xs={12}>
              <Divider />
            </Grid>
          </Box>
          <Grid component='section' container display='flex'>
            {FOOTER_LINKS_CONTENT.map((linkContent, index) => (
              <Grid
                key={`footer-link-content-${index}`}
                item
                xs={12}
                md={4}
                lg={3}
              >
                {linkContent.map((childLinkContent, childIndex) => (
                  <FooterExpansionPanel
                    key={`footer-link-content-${index}-child-${childIndex}`}
                    disabled={windowIsMD}
                    {...childLinkContent}
                  />
                ))}
              </Grid>
            ))}
            <Grid
              container
              item
              xs={12}
              md={12}
              lg={3}
              direction='row'
              display='flex'
            >
              <Grid item xs={12} md={4} lg={12} display='flex'>
                <FooterInfoSection
                  ariaControls='cin-content'
                  heading={CIN_HEADING}
                  id='cin-header'
                  value={CIN_VALUE}
                />
              </Grid>
              <Grid item xs={12} md={4} lg={12} display='flex'>
                <FooterInfoSection
                  ariaControls='registered-content'
                  heading={REGISTERED_OFFICE_HEADING}
                  id='registered-header'
                  value={REGISTERED_OFFICE_VALUE}
                />
              </Grid>
              <Grid item xs={12} md={4} lg={12} display='flex'>
                <FooterInfoSection
                  ariaControls='corporate-content'
                  heading={CORPORATE_ADDRESS_HEADING}
                  id='corporate-header'
                  value={CORPORATE_ADDRESS_VALUE}
                />
              </Grid>
            </Grid>
          </Grid>
        </Box>
        <Box
          display='flex'
          flexWrap='wrap'
          maxWidth='100%'
          bgcolor='denary.main'
          justifyContent='space-between'
          color='shade0.main'
          fontSize='1.4rem'
          px={{ xs: 4, md: 28, lg: 42 }}
          py={{ xs: 6, md: 8 }}
          mt={{ xs: 7, md: 22 }}
        >
          <FooterCopyright copyrightText={COPYRIGHT_TEXT} />
          <FooterSocialMediaItems />
        </Box>
      </Box>
    );
  }
}

Footer.propTypes = {
  windowIsMD: PropTypes.bool.isRequired,
};

export default withWindowDimensions(Footer);
