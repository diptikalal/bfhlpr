import React, { Fragment, PureComponent } from "react";
import PropTypes from "prop-types";

import { Box, Grid, Link } from "@material-ui/core";

import Image from "../../Components/Image/image.component";

class Partners extends PureComponent {
  render() {
    const {
      description,
      partners,
      title
    } = this.props;

    return (
      <Fragment key="legacy">
        <Box component="div" margin="0 auto" display="flex" flexDirection="column" textAlign="center">
          <Box component="h2">{title}</Box>
          <Box component="h3">{description}</Box>
          {
            partners.length > 0
            && (
              <Grid container component="section" justify={"space-around"} alignItems={"center"} className="no-margin">
                {
                  partners.map(({
                    partner_logo: {
                      name,
                      sizes: {
                        thumbnail
                      }
                    },
                    partner_website_link
                  }, index) => (
                      <Grid key={`partner-${index}`} item xs={3}>
                        <Link href={partner_website_link} color="inherit" className="align-items-center">
                          <Image alt={name} src={thumbnail} />
                        </Link>
                      </Grid>
                    ))
                }
              </Grid>
            )
          }
        </Box>
      </Fragment>
    );
  }
}

Partners.propTypes = {
  description: PropTypes.string.isRequired,
  partners: PropTypes.array.isRequired,
  title: PropTypes.string.isRequired
};

export default Partners;
