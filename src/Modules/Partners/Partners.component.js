import React, { Fragment, PureComponent } from "react";
import PropTypes from "prop-types";

import { Grid, Link } from "@material-ui/core";

import Image from "../../Components/Image/image.component";

class Partners extends PureComponent {
  render() {
    const {
      description,
      partners,
      title
    } = this.props;

    return (
      <Fragment key="partners">
        <h2 id="partners">{title}</h2>
        <h3>{description}</h3>
        {
          partners.length > 0
          && (
            <Grid container component="section" justify={"space-around"} alignItems={"center"} className="no-margin">
              {
                partners.map(({
                  partner_logo: {
                    name,
                    url
                  },
                  partner_website_link
                }, index) => (
                    <Grid key={`partner-${index}`} item xs={3}>
                      <Link href={partner_website_link} color="inherit" className="align-items-center">
                        <Image alt={name} src={url} />
                      </Link>
                    </Grid>
                  ))
              }
            </Grid>
          )
        }
      </Fragment>
    );
  }
}

Partners.propTypes = {
  description: PropTypes.string.isRequired,
  partners: PropTypes.array.isRequired,
  title: PropTypes.string.isRequired
};

export default Partners;
