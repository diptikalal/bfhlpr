import React, { PureComponent } from "react";

import PropTypes from "prop-types";
import { Box } from "@material-ui/core";

import logoSrc from '../../Assets/Images/static/healthrx-bajaj.png'
import Image from "../../Components/Image/image.component";

class FooterLogo extends PureComponent {
    render() {
        const {
            component,
            maxWidth,
            ml,
            my,
            py
        } = this.props;

        return (
            <Box
                component={component}
                maxWidth={maxWidth}
                ml={ml}
                my={my}
                py={py}
            >
                <Image
                    alt="HealthRx"
                    src={logoSrc}
                />
            </Box>
        );
    }
}


FooterLogo.propTypes = {
    component: PropTypes.string,
    maxWidth: PropTypes.shape({
        xs: PropTypes.number,
        md: PropTypes.number,
        lg: PropTypes.number
    }),
    ml: PropTypes.shape({
        xs: PropTypes.number,
        md: PropTypes.number
    }),
    my: PropTypes.number,
    py: PropTypes.number
};

FooterLogo.defaultProps = {
    component: "figure",
    maxWidth: { xs: 120, md: 150, lg: 185 },
    ml: { xs: 4, md: 0 },
    my: 0,
    py: 11
};

export default FooterLogo;