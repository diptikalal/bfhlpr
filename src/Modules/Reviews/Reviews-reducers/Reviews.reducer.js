import { REVIEWS } from "../Reviews-constants/Reviews.constants";

const reviewsReducer = (state = {}, action) => {
  switch (action.type) {
    case REVIEWS.REVIEW_LOADING:
      return {
        ...state,
        loading: true,
      };
    case REVIEWS.REVIEW_SUCCESS:
      return {
        ...state,
        testimonial: action.Reviews,
        loading: false,
      };
    default:
      return state;
  }
};

export default reviewsReducer;
