import React, { Component } from "react";

class ReviewsCard extends Component {
  render() {
    const { cardContents } = this.props;
    console.log("photo", cardContents.author_photo);
    return (
      <div>
        <h1>Reviews slider</h1>
        <h2>{cardContents.author_name}</h2>
        <h2>{cardContents.testiminial_content}</h2>
      </div>
    );
  }
}

export default ReviewsCard;
