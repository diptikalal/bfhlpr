import { connect } from 'react-redux';
import Reviews from './Reviews.component';
import {loadReviews } from './Reviews-actions/Reviews.action';


const mapStateToProps = ({ isLoading,Reviews:{testimonial = []} , error }) => ({
    isLoading,
    testimonial,
    error,
    
});
const mapDispatchToProps = dispatch => ({
    loadReviews: () => dispatch(loadReviews()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Reviews);
