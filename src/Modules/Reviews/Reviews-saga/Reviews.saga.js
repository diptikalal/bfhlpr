import { put, call, takeEvery } from 'redux-saga/effects';

import { setReviews, setError } from '../Reviews-actions/Reviews.action';
import { REVIEWS } from '../Reviews-constants/Reviews.constants';
import { fetchReviews } from '../../../Services/Api.services';

export function* handleReviewsLoad() {
    try {
        const Reviews = yield call(fetchReviews);
        yield put(setReviews(Reviews));
    } catch (error) {
        yield put(setError(error.toString()));
    }
}

export default function* watchReviewsLoad() {
    yield takeEvery(REVIEWS.REVIEW_LOADING, handleReviewsLoad);
} 
