import { REVIEWS } from '../Reviews-constants/Reviews.constants';

const loadReviews = () => ({
    type: REVIEWS.REVIEW_LOADING,
});

const setReviews = Reviews => ({
    type: REVIEWS.REVIEW_SUCCESS,
    Reviews,
});

const setError = error => ({
    type: REVIEWS.REVIEW_ERROR,
    error,
});



export {
    loadReviews,
    setReviews,
    setError
};
