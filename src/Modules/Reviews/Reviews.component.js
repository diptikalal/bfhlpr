import React, { Component } from "react";

import Carousel from "../../Components/Carousel/Carousel.component";

class Reviews extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cardSlider: true,
    };
  }

  componentDidMount() {
    console.log(this.props);
    this.props.loadReviews();
  }

  render() {
    const {
      testimonial: { bfhl_testimonial },
    } = this.props;
    const { cardSlider } = this.state;

    return bfhl_testimonial ? (
      <Carousel sliderData={bfhl_testimonial} cardSlider={cardSlider} />
    ) : null;
  }
}

export default Reviews;
