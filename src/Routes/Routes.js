import AboutUs from "../Pages/AboutUs/AboutUs.container";
import CompleteHealthSolutionPackage from "../Pages/CompleteHealthSolutionPackage/CompleteHealthSolutionPackage.component";
import CustomerService from "../Pages/CustomerService/CustomerService.component";
import DoctorRx from "../Pages/DoctorRx/DoctorRx.component";
import HealthRx from "../Pages/HealthRx/HealthRx.component";
import HospicarePlusPackage from "../Pages/HospicarePlusPackage/HospicarePlusPackage.component";
import ManagedCare from "../Pages/ManagedCare/ManagedCare.component";
import Home from "../Pages/Home/Home.container";
import PrivacyPolicy from "../Pages/PrivacyPolicy/PrivacyPolicy.component";
import ProHealthSolutionPackage from "../Pages/ProHealthSolutionPackage/ProHealthSolutionPackage.component";
import RubyHallClinicLoyaltyCard from "../Pages/RubyHallClinicLoyaltyCard/RubyHallClinicLoyaltyCard.component";
import Sitemap from "../Pages/Sitemap/Sitemap.component";
import SuperHealthSolutionPackage from "../Pages/SuperHealthSolutionPackage/SuperHealthSolutionPackage.component";
import TermsAndConditions from "../Pages/TermsAndConditions/TermsAndConditions.component";
import WorkWithUs from "../Pages/WorkWithUs/WorkWithUs.component";
import NotFound from "../Pages/NotFound/NotFound.component";

export default [
    {
        path: "/",
        exact: true,
        component: Home,
        name : "Home"
    },
    {
        path: "/about-us",
        component: AboutUs,
        name: "AboutUs"
    },
    {
        path: "/complete-health-solution-package",
        component: CompleteHealthSolutionPackage,
    },
    {
        path: "/customer-service",
        component: CustomerService
    },
    {
        path: "/doctor-rx",
        component: DoctorRx
    },
    {
        path: "/health-rx",
        component: HealthRx
    },
    {
        path: "/hospicare-plus-package",
        component: HospicarePlusPackage
    },
    {
        path: "/managed-care",
        component: ManagedCare
    },
    {
        path: "/privacy-policy",
        component: PrivacyPolicy
    },
    {
        path: "/pro-health-solution-package",
        component: ProHealthSolutionPackage
    },
    {
        path: "/rhc-loyalty-card",
        component: RubyHallClinicLoyaltyCard
    },
    {
        path: "/sitemap",
        component: Sitemap
    },
    {
        path: "/super-health-solution-package",
        component: SuperHealthSolutionPackage
    },
    {
        path: "/terms-and-conditions",
        component: TermsAndConditions
    },
    {
        path: "/work-with-us",
        component: WorkWithUs
    },
    {
        component: NotFound
    }
    // {
    //   path: "/child/:id",
    //   component: Child,
    //   routes: [
    //     {
    //       path: "/child/:id/grand-child",
    //       component: GrandChild
    //     }
    //   ]
    // }
  ];