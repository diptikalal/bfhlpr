import React, { Component, Fragment } from 'react';
import { Helmet } from "react-helmet";

class DoctorRx extends Component {
    render() {
        return (
            <Fragment>
                <Helmet>
                    <title>DoctorRx</title>
                    <meta name="description" content="DoctorRx" />
                </Helmet>
                <h2>This is DoctorRx</h2>
            </Fragment>
        );
    }
}

export default DoctorRx;