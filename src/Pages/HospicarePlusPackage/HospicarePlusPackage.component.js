import React, { Component, Fragment } from 'react';
import { Helmet } from "react-helmet";

class HospicarePlusPackage extends Component {
    render() { 
        return ( 
            <Fragment>
                <Helmet>
                    <title>Hospicare Plus Package</title>
                    <meta name="description" content="Hospicare Plus Package" />
                </Helmet>
                <h2>This is Hospicare Plus Package</h2>
            </Fragment>
         );
    }
}
 
export default HospicarePlusPackage;