import React, { Component, Fragment } from 'react';
import { Helmet } from "react-helmet";

class Sitemap extends Component {
    render() { 
        return ( 
            <Fragment>
                <Helmet>
                    <title>Sitemap</title>
                    <meta name="description" content="Sitemap" />
                </Helmet>
                <h2>This is Sitemap</h2>
            </Fragment>
         );
    }
}
 
export default Sitemap;