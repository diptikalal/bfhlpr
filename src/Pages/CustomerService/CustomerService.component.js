import React, { Component, Fragment } from 'react';
import { Helmet } from "react-helmet";

class CustomerService extends Component {
    render() {
        return (
            <Fragment>
                <Helmet>
                    <title>Customer Service</title>
                    <meta name="description" content="Customer Service" />
                </Helmet>
                <h2>This is Customer Service</h2>
            </Fragment>
        );
    }
}

export default CustomerService;