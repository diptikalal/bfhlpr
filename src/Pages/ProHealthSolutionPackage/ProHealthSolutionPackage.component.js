import React, { Component, Fragment } from 'react';
import { Helmet } from "react-helmet";

class ProHealthSolutionPackage extends Component {
    render() { 
        return ( 
            <Fragment>
                <Helmet>
                    <title>Pro Health Solution Package</title>
                    <meta name="description" content="Pro Health Solution Package" />
                </Helmet>
                <h2>This is Pro Health Solution Package</h2>
            </Fragment>
         );
    }
}
 
export default ProHealthSolutionPackage;