import React, { Component, Fragment } from 'react';
import { Helmet } from "react-helmet";

class SuperHealthSolutionPackage extends Component {
    render() { 
        return ( 
            <Fragment>
                <Helmet>
                    <title>Super Health Solution Package</title>
                    <meta name="description" content="Super Health Solution Package" />
                </Helmet>
                <h2>This is Super Health Solution Package</h2>
            </Fragment>
         );
    }
}
 
export default SuperHealthSolutionPackage;