import React, { Component, Fragment } from 'react';
import { Helmet } from "react-helmet";

class WorkWithUs extends Component {
    render() { 
        return ( 
            <Fragment>
                <Helmet>
                    <title>Work With Us</title>
                    <meta name="description" content="Work With Us" />
                </Helmet>
                <h2>This is Work With Us</h2>
            </Fragment>
         );
    }
}
 
export default WorkWithUs;