import React, { Component, Fragment } from 'react';
import { Helmet } from "react-helmet";

class ManagedCare extends Component {
    render() { 
        return ( 
            <Fragment>
                <Helmet>
                    <title>Managed Care</title>
                    <meta name="description" content="Managed Care" />
                </Helmet>
                <h2>This is Managed Care</h2>
            </Fragment>
         );
    }
}
 
export default ManagedCare;