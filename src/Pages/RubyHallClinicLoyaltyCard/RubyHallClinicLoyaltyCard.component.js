import React, { Component, Fragment } from 'react';
import { Helmet } from "react-helmet";

class RubyHallClinicLoyaltyCard extends Component {
    render() { 
        return ( 
            <Fragment>
                <Helmet>
                    <title>Ruby Hall Clinic Loyalty Card</title>
                    <meta name="description" content="Ruby Hall Clinic Loyalty Card" />
                </Helmet>
                <h2>This is Ruby Hall Clinic Loyalty Card</h2>
            </Fragment>
         );
    }
}
 
export default RubyHallClinicLoyaltyCard;