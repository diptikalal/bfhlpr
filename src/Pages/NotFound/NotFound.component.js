import React from 'react';

const NotFound = ({ staticContext = {} }) => {
    staticContext.status = 404;
    return <h2>Page Not Found!!!</h2>
};

export default NotFound;
