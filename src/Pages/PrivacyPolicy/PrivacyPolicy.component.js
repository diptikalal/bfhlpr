import React, { Component, Fragment } from 'react';
import { Helmet } from "react-helmet";

class PrivacyPolicy extends Component {
    render() { 
        return ( 
            <Fragment>
                <Helmet>
                    <title>Privacy Policy</title>
                    <meta name="description" content="Privacy Policy" />
                </Helmet>
                <h2>This is Privacy Policy</h2>
            </Fragment>
         );
    }
}
 
export default PrivacyPolicy;