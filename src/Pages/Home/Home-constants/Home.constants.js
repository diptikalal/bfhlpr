const DESCRIPTION = 'Being Healthy has never been so easy. Find the best Doctors, set your Medicine Reminders & store your Health Records at Your Fingertips with the help of Health Rx.';
const IMAGES = [
  {
    imagesrc_lg: require("../../../Assets/Images/static/slider1-image-lg.jpg"),
    imagesrc_sm: require("../../../Assets/Images/static/slider1-image-sm.jpg"),
    alt: "image1",
    figcaption: "Book a COVID-19 Test",
    figcaption1: "Through our",
    figcaption2: "HelathRx",
    figcaption3: "App.",
    subTitle: "All your healthcare needs solved. Right here.",
  },
  {
    imagesrc_lg: require("../../../Assets/Images/static/slider2-image-lg.jpg"),
    imagesrc_sm: require("../../../Assets/Images/static/slider2-image-sm.jpg"),
    alt: "image2",
    figcaption: "Book a COVID-19 Test",
    figcaption1: "Through our",
    figcaption2: "HelathRx",
    figcaption3: "App.",
    subTitle: "All your healthcare needs solved. Right here.",
  }
];
const TITLE = 'Health Rx - Your Personalised Healthcare Platform';

export {
  DESCRIPTION,
  IMAGES,
  TITLE
};