import React, { Component } from 'react';
import { Helmet } from 'react-helmet';
import { Box } from '@material-ui/core';
import WhoAreWeWorkWithUs from '../../Modules/WhoAreWeWorkWithUs/WhoAreWeWorkWithUs.component';
import Reviews from '../../Modules/Reviews/Reviews.container';
import DownloadTheApp from '../../Modules/DownloadTheApp/DownloadTheApp.component';
import withLayout from '../../Components/Layout/Layout.component';
import Banner from '../../Modules/Banner/Banner.component';
import PreferredChoice from '../../Modules/Homepage-preferred-choice';
import {
  DESCRIPTION,
  IMAGES,
  TITLE
} from './Home-constants/Home.constants';

class HomeComponent extends Component {
  render() {
    return (
      <article id='home'>
        <Helmet>
          <title>{TITLE}</title>
          <meta name='description' content={DESCRIPTION} />
        </Helmet>
        <Box component="section" height="100vh">
          <Banner sliderData={IMAGES} showDownloadButton />
        </Box>
        <PreferredChoice />
        <DownloadTheApp />
        <WhoAreWeWorkWithUs />
        <Reviews />
      </article>
    );
  }
}

export default withLayout(HomeComponent);
