import React, { Component, Fragment } from 'react';
import { Helmet } from "react-helmet";

class TermsAndConditions extends Component {
    render() { 
        return ( 
            <Fragment>
                <Helmet>
                    <title>Terms And Conditions</title>
                    <meta name="description" content="Terms And Conditions" />
                </Helmet>
                <h2>This is Terms And Conditions</h2>
            </Fragment>
         );
    }
}
 
export default TermsAndConditions;