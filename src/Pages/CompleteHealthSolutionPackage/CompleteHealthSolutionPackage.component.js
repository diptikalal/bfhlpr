import React, { Component, Fragment } from 'react';
import { Helmet } from "react-helmet";

class CompleteHealthSolutionPackage extends Component {
    render() {
        return (
            <Fragment>
                <Helmet>
                    <title>Complete Health Solution Package</title>
                    <meta name="description" content="Complete Health Solution Package" />
                </Helmet>
                <h2>This is Complete Health Solution Package</h2>
            </Fragment>
        );
    }
}

export default CompleteHealthSolutionPackage;