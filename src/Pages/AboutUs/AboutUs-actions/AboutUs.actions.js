import { 
    FETCH_ABOUT_US_CONTENT_FULFILLED,
    FETCH_ABOUT_US_CONTENT_PENDING,
    FETCH_ABOUT_US_CONTENT_REJECTED
 } from "../AboutUs-constants/AboutUs.constants";

const fetchAboutUsContent = () => ({
    type: FETCH_ABOUT_US_CONTENT_PENDING,
});

const fetchAboutUsContentSuccess = payload => ({
    type: FETCH_ABOUT_US_CONTENT_FULFILLED,
    payload,
});

const fetchAboutUsContentError = payload => ({
    type: FETCH_ABOUT_US_CONTENT_REJECTED,
    payload,
});

export {
    fetchAboutUsContent,
    fetchAboutUsContentSuccess,
    fetchAboutUsContentError
};