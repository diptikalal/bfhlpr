import React, { Component, Fragment } from "react";
import { Helmet } from "react-helmet";
import { Breadcrumbs, Typography } from "@material-ui/core";
import Routes from "../../Routes/Routes";
import Link from "../../Components/Link/Link.component";

import GotoTab from "../../Modules/Goto-Tabs/Goto-Tabs.component";
import Banner from "../../Modules/Banner/Banner.component";
import Partners from "../../Modules/Partners/Partners.component";
import Legacy from "../../Modules/Legacy/Legacy.component";
import Loader from "../../Components/Loader/Loader.component";
import withLayout from "../../Components/Layout/Layout.component";
import {
  DESCRIPTION,
  IMAGES,
  PARTNERS_DESCRIPTION,
  PARTNERS_TITLE,
  TITLE,
} from "./AboutUs-constants/AboutUs.constants";

class AboutUs extends Component {
  componentDidMount() {
    const { fetchAboutUsContent } = this.props;
    fetchAboutUsContent();
  }

  render() {
    const {
      content: {
        bfhl_legacy_description = "",
        bfhl_legacy_title = "",
        bfhl_partner = [],
      },
      contentError,
      loaded,
    } = this.props;

    return (
      <Fragment key="aboutUs">
        <Helmet>
          <title>{TITLE}</title>
          <meta name="description" content={DESCRIPTION} />
        </Helmet>

        {!loaded && <Loader />}
        {loaded &&
          ((contentError && (
            <Fragment key="aboutUsError">{contentError}</Fragment>
          )) ||
            (!contentError && (
              <Fragment key="aboutUsSuccess">
                <Banner sliderData={IMAGES} />
                <Breadcrumbs aria-label="breadcrumb">
                  <Link Routes={Routes[0]} />
                  <Link Routes={Routes[1]} />
                </Breadcrumbs>
                <GotoTab />
                <Legacy
                  description={bfhl_legacy_description}
                  title={bfhl_legacy_title}
                />
                <Partners
                  description={PARTNERS_DESCRIPTION}
                  partners={bfhl_partner}
                  title={PARTNERS_TITLE}
                />
              </Fragment>
            )))}
      </Fragment>
    );
  }
}

export default withLayout(AboutUs);
