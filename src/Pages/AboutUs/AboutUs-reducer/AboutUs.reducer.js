import {
    FETCH_ABOUT_US_CONTENT_FULFILLED,
    FETCH_ABOUT_US_CONTENT_PENDING,
    FETCH_ABOUT_US_CONTENT_REJECTED
} from "../AboutUs-constants/AboutUs.constants";

const initialState = {
    content: {},
    contentError: '',
    loaded: false
};

const aboutUsReducer = (state = initialState, action = {}) => {
    const {
        payload,
        type
    } = action;

    switch (type) {
        case FETCH_ABOUT_US_CONTENT_PENDING:
            return {
                ...state,
                loaded: false,
            };
        case FETCH_ABOUT_US_CONTENT_FULFILLED:
            return {
                ...state,
                content: payload,
                contentError: '',
                loaded: true,
            };
        case FETCH_ABOUT_US_CONTENT_REJECTED:
            return {
                ...state,
                content: {},
                contentError: payload,
                loaded: true
            };
        default:
            return state;
    }
};

export default aboutUsReducer;
