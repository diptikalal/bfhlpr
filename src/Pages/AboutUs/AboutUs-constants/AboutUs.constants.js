const DESCRIPTION = 'About Us';
const DNA_TITLE = 'Dive into our DNA';
const DNA_WHY_US_TITLE_1 = "We saw a problem we could fix.";
const DNA_WHY_US_TITLE_2 = "We evolved into HealthRx.";
const DNA_WHY_US_TITLE_3 = "We are more than just an app or a website.";
const DNA_WHY_US_TITLE_4 = "We are your Personalized Health Manager.";
const FETCH_ABOUT_US_CONTENT_FULFILLED = "FETCH_ABOUT_US_CONTENT_FULFILLED";
const FETCH_ABOUT_US_CONTENT_PENDING = "FETCH_ABOUT_US_CONTENT_PENDING";
const FETCH_ABOUT_US_CONTENT_REJECTED = "FETCH_ABOUT_US_CONTENT_REJECTED";
const IMAGES = [
    {
        imagesrc_lg: require("../../../Assets/Images/static/slider1-image-lg.jpg"),
        imagesrc_sm: require("../../../Assets/Images/static/slider1-image-sm.jpg"),
        alt: "image1",
        figcaption: "About Us",
        subTitle: "We come from the house of Bajaj Finserv Limited.",
    }
];
const PARTNERS_DESCRIPTION = "We have collaborated with some of the best healthcare providers with whom we have crafted an edge with our offerings.";
const PARTNERS_TITLE = "Our partners";
const LEGACY_TITLE = "Our legacy. Our future.";
const TITLE = 'About Us';

export { 
    DESCRIPTION,
    DNA_TITLE,
    DNA_WHY_US_TITLE_1,
    DNA_WHY_US_TITLE_2,
    DNA_WHY_US_TITLE_3,
    DNA_WHY_US_TITLE_4,
    FETCH_ABOUT_US_CONTENT_FULFILLED,
    FETCH_ABOUT_US_CONTENT_PENDING,
    FETCH_ABOUT_US_CONTENT_REJECTED,
    IMAGES,
    LEGACY_TITLE,
    PARTNERS_DESCRIPTION,
    PARTNERS_TITLE,
    TITLE
 };