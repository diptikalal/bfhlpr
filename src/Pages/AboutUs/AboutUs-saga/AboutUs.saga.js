import { put, call, takeEvery } from 'redux-saga/effects';

import { 
    fetchAboutUsContentError,
    fetchAboutUsContentSuccess
 } from '../AboutUs-actions/AboutUs.actions';
import { FETCH_ABOUT_US_CONTENT_PENDING } from '../AboutUs-constants/AboutUs.constants';
import ApiServices from '../../../Services/Api.services';

export function* handlefetchAboutUsContent() {
    try {
        const content = yield call(ApiServices('/index.php/wp-json/wp/v2/pages/about-us').get);
        yield put(fetchAboutUsContentSuccess(content));
    } catch (error) {
        yield put(fetchAboutUsContentError(error.toString()));
    }
}

export default function* watchFetchAboutUsContent() {
    yield takeEvery(FETCH_ABOUT_US_CONTENT_PENDING, handlefetchAboutUsContent);
} 
