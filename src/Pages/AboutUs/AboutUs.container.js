import { connect } from 'react-redux';
import { bindActionCreators } from "redux";

import AboutUs from './AboutUs.component';
import { fetchAboutUsContent } from "./AboutUs-actions/AboutUs.actions";

const mapStateToProps = ({
    AboutUs: {
        content = {},
        contentError = '',
        loaded = false
    }
}) => ({
    content,
    contentError,
    loaded
});

const mapDispatchToProps = dispatch => bindActionCreators({
    fetchAboutUsContent
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AboutUs);