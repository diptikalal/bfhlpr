import React, { Component, Fragment } from 'react';
import { Helmet } from "react-helmet";

import { Breadcrumbs } from "@material-ui/core";

import Routes from "../../Routes/Routes";
import Link from '../../Components/Link/Link.component';
import Banner from '../../Modules/Banner/Banner.component';
import DNA from '../../Modules/DNA/DNA.component';
import Partners from '../../Modules/Partners/Partners.component';
import Legacy from '../../Modules/Legacy/Legacy.component';
import Loader from '../../Components/Loader/Loader.component';
import withLayout from '../../Components/Layout/Layout.component';
import {
    DESCRIPTION,
    DNA_TITLE,
    DNA_WHY_US_TITLE_1,
    DNA_WHY_US_TITLE_2,
    DNA_WHY_US_TITLE_3,
    DNA_WHY_US_TITLE_4,
    IMAGES,
    LEGACY_TITLE,
    PARTNERS_DESCRIPTION,
    PARTNERS_TITLE,
    TITLE
} from "./AboutUs-constants/AboutUs.constants";

class AboutUs extends Component {
    componentDidMount() {
        const { fetchAboutUsContent } = this.props;
        fetchAboutUsContent();
    }

    render() {
        const {
            content: {
                bfhl_dive_into_dna_description = '',
                bfhl_dive_into_dna_title = '',
                bfhl_legacy_description = '',
                bfhl_legacy_title = '',
                bfhl_partner = [],
                bfhl_why_us: {
                    point_one_description = '',
                    point_one_title = '',
                    point_three_description = '',
                    point_three_title = '',
                    point_two_description = '',
                    point_two_title = ''
                } = {}
            },
            contentError,
            loaded
        } = this.props;
        const whyUsReasons = [
            {
                description: point_one_description,
                title: point_one_title
            },
            {
                description: point_two_description,
                title: point_two_title
            },
            {
                description: point_three_description,
                title: point_three_title
            }
        ];

        return (
            <Fragment key="aboutUs">
                <Helmet>
                    <title>{TITLE}</title>
                    <meta name="description" content={DESCRIPTION} />
                </Helmet>
                <Breadcrumbs aria-label="breadcrumb">
                    <Link Routes={Routes[0]} />
                    <Link Routes={Routes[1]} />
                </Breadcrumbs>
                {!loaded && <Loader />}
                {
                    loaded && (
                        (
                            contentError && <Fragment key="aboutUs">{contentError}</Fragment>
                        )
                        || (
                            !contentError && (
                                <Fragment key="aboutUs">
                                    <Banner sliderData={IMAGES} />
                                    <Legacy
                                        description={bfhl_legacy_description}
                                        subTitle={bfhl_legacy_title}
                                        title={LEGACY_TITLE}
                                    />
                                    <DNA
                                        description={bfhl_dive_into_dna_description}
                                        subTitle={bfhl_dive_into_dna_title}
                                        title={DNA_TITLE}
                                        whyUsTitle1={DNA_WHY_US_TITLE_1}
                                        whyUsTitle2={DNA_WHY_US_TITLE_2}
                                        whyUsTitle3={DNA_WHY_US_TITLE_3}
                                        whyUsTitle4={DNA_WHY_US_TITLE_4}
                                        whyUsReasons={whyUsReasons}
                                    />
                                    <Partners
                                        description={PARTNERS_DESCRIPTION}
                                        partners={bfhl_partner}
                                        title={PARTNERS_TITLE}
                                    />
                                </Fragment>
                            )
                        )
                    )
                }
            </Fragment>
        );
    }
}

export default withLayout(AboutUs);