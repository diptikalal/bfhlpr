import React, { Component, Fragment } from 'react';
import { Helmet } from "react-helmet";

class HealthRx extends Component {
    render() { 
        return ( 
            <Fragment>
                <Helmet>
                    <title>HealthRx</title>
                    <meta name="description" content="HealthRx" />
                </Helmet>
                <h2>This is HealthRx</h2>
            </Fragment>
         );
    }
}
 
export default HealthRx;