import { put, call, takeEvery, select } from 'redux-saga/effects';

import { setImages, setError } from '../Login-actions/Login.action';
import { IMAGES } from '../Login-constants/Login.constants';
import { fetchImages } from '../../../Services/Api.services';

export const getPage = state => state.nextPage;

export function* handleImagesLoad() {
    try {
        const page = yield select(getPage);
        const images = yield call(fetchImages, page);
        yield put(setImages(images));
    } catch (error) {
        yield put(setError(error.toString()));
    }
}

export default function* watchImagesLoad() {
    yield takeEvery(IMAGES.LOAD, handleImagesLoad);
} 
