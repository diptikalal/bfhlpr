import { IMAGES } from '../Login-constants/Login.constants';

const imagesReducer = (state = {}, action) => {
    if (action.type === IMAGES.LOAD_SUCCESS) {
        return {...state, images:action.images};
    }
    return state;
};

export default imagesReducer;
