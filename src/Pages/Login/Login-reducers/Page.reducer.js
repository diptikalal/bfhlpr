import { 
    INCREMENT_VALUE,
    IMAGES,
    INITIAL_STATE_VALUE
} from '../Login-constants/Login.constants';

const pageReducer = (state = INITIAL_STATE_VALUE, action) => {
    switch (action.type) {
        case IMAGES.LOAD_SUCCESS:
            return state + INCREMENT_VALUE;
        default:
            return state;
    }
};

export default pageReducer;
