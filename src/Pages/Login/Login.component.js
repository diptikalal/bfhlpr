import React, { Component } from 'react';

import Button from './Button.component';

export default class ImageGrid extends Component {
    componentDidMount() {
      console.log(this.props);
        this.props.loadImages();
    }

    render() {
        const { isLoading, images, loadImages, error } = this.props;
        return (
            <div className="content">
                <section className="grid">
                    {images.map(image => (
                        <div
                            key={image.id}
                            className={`item item-${Math.ceil(
                                image.height / image.width,
                            )}`}
                        >
                            <img
                                src={image.urls.small}
                                alt={image.user.username}
                            />
                        </div>
                    ))}
                </section>
                {error && <div className="error">{JSON.stringify(error)}</div>}
                <Button
                    key="load-more-images-button"
                    onClick={() => !isLoading && loadImages()}
                    loading={isLoading}
                >
                    Load More
                </Button>
            </div>
        );
    }
}
