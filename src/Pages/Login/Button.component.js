import React, { Component } from 'react';

class Button extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { children, loading, ...props } = this.props;

        return (
            <button className="button" disabled={loading} {...props}>
                {
                    loading
                        ? 'Loading...'
                        : children
                }
            </button>
        );
    }
}

Button.defaultProps = {
    loading: false,
};

export default Button;
