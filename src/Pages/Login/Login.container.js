import { connect } from 'react-redux';
import Login from './Login.component';
import { loadImages } from './Login-actions/Login.action';

const mapStateToProps = ({ isLoading,Login:{images = []} , error }) => ({
     isLoading,
     images,
     error,
     
 });
 
 const mapDispatchToProps = dispatch => ({
     loadImages: () => dispatch(loadImages()),
 });
 
 export default connect(
     mapStateToProps,
     mapDispatchToProps,
 )(Login);
 


 