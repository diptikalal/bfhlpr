import React from 'react';
import { Helmet } from 'react-helmet';
import Button from '@material-ui/core/Button';

// DUMMY COMPONENT USED FOR REPRESENTATIONAL PURPOSE ONLY. CAN BE REMOVED ENTIRELY OR UPDATED
const Dashboard = () => (
  <React.Fragment>
    <Helmet>
      <title>Dashboard</title>
    </Helmet>
    <h2>Welcome to Dashboard</h2>
    <Button variant="contained" color="primary">
      Hello World
    </Button>
  </React.Fragment>
);

export default Dashboard;
