import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import { StylesProvider, ThemeProvider } from '@material-ui/core/styles';
import { Provider } from 'react-redux';
import App from './App';
import * as serviceWorker from './serviceWorker';
import theme from './Assets/theme';

import './index.css';
import './Assets/Styles/main.scss';
import configureStore from '../src/Store/index';

const Main = () => {
  const store = configureStore();
  React.useEffect(() => {
    const jssStyles = document.querySelector('#jss-server-side');
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles);
    }
  }, []);

  return (
    <Provider store={store}>
      <BrowserRouter>
        <React.StrictMode>
          <ThemeProvider theme={theme}>
            <StylesProvider injectFirst>
              <App />
            </StylesProvider>
          </ThemeProvider>
        </React.StrictMode>
      </BrowserRouter>
    </Provider>
  );
};

ReactDOM.hydrate(<Main />, document.getElementById('root'));

serviceWorker.register();
