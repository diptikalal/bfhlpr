import React from "react";
import { renderToString } from "react-dom/server";
import { StaticRouter } from "react-router-dom";
import { Provider } from "react-redux";
import serialize from "serialize-javascript";
import { Helmet } from "react-helmet";
import { ServerStyleSheets, StylesProvider, ThemeProvider } from "@material-ui/core/styles";
import path from "path";
import fs from "fs";

import App from "../../src/App";
import theme from "../../src/Assets/theme";
import configureStore from "../../src/Store/index";

export default (req, res, data) => {
  const filePath = path.resolve(".", "build", "index.html");
  const context = { data };
  fs.readFile(filePath, "utf8", (err, htmlData) => {
    if (err) {
      console.error("err", err);
      return res.status(404).end();
    }

    if (context.status === 404) {
      res.status(404);
    }

    if (context.url) {
      return res.redirect(301, context.url);
    }

    const store = configureStore();
    const sheets = new ServerStyleSheets();
    const content = renderToString(
      sheets.collect(
        <Provider store={store}>
          <StaticRouter location={req.url} context={context}>
            <React.StrictMode>
              <ThemeProvider theme={theme}>
                <StylesProvider injectFirst>
                  <App />
                </StylesProvider>
              </ThemeProvider>
            </React.StrictMode>
          </StaticRouter>
        </Provider>
      )
    );
    const css = sheets.toString();

    const helmet = Helmet.renderStatic();

    let serverHTMLData = htmlData
      .replace(`<div id="root"></div>`, `<div id="root">${content}</div>`)
      .replace(
        "</body>",
        `<script>window.__ROUTE_DATA__ = ${serialize(data)}</script></body>`
      );

    serverHTMLData = serverHTMLData.replace(
      `<head>`,
      `<head>
        ${helmet.title.toString()}
        ${helmet.meta.toString()}
        ${helmet.link.toString()}
        ${helmet.script.toString()}
        <style id="jss-server-side">${css}</style>
      `
    );

    // FOLLOWING CODE NEEDS TO BE UPDATED TO RESOLVE ISSUE OF CONTENT SECURITY POLICY.
    // Material UI component APIs not working. No inline script executes
    // serverHTMLData = serverHTMLData.replace(
    //   '<head>',
    //   `<head>
    //     <meta property="csp-nonce" content=${res.locals.nonce}/>
    //   `
    // );
    // serverHTMLData = serverHTMLData.replace(
    //   '<style id="jss-server-side">',
    //   `<style id="jss-server-side" nonce=${res.locals.nonce}>`
    // );

    res.send(serverHTMLData);
  });
};
