import express from 'express';
import renderer from './Helper/Renderer';
import path from 'path';
// import helmet from 'helmet';
// import uuidv4 from 'uuid/v4';

import Routes from '../src/Routes/Routes';
import { matchRoutes } from 'react-router-config';

const app = express();

app.use(express.static(path.resolve('.', 'build'), { index: false }));

// app.use((req, res, next) => {
//   res.locals.nonce = Buffer.from(uuidv4()).toString('base64');
//   next();
// });

// app.use(
//   helmet.contentSecurityPolicy({
//     directives: {
//       defaultSrc: ["'self'"],
//       styleSrc: ["'self'", (req, res) => `'nonce-${res.locals.nonce}'`],
//       scriptSrc: ["'self'", (req, res) => `'nonce-${res.locals.nonce}'`],
//     },
//   })
// );

app.get('*', (req, res) => {
  const matchingRoutes = matchRoutes(Routes, req.url);
  let promises = [];
  matchingRoutes.forEach((route) => {
    promises.push(route.loadData ? route.loadData() : Promise.resolve(null));
  });

  Promise.all(promises).then((data) => {
    renderer(req, res, data);
  });
});

const port  = process.env.PORT || 3000;

app.listen(port, () => {
  console.log(`Listening on port ${port}...`);
});
