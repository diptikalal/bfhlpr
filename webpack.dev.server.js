const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
  mode: "development",
  target: "node",
  entry: "./Server/index.js",
  output: {
    path: path.resolve(__dirname, "server-build"),
    filename: "index.js",
  },
  resolve: {
    extensions: [".js", ".jsx"],
  },
  plugins: [new MiniCssExtractPlugin()],
  module: {
    rules: [
      {
        test: /\.(js|jsx)?$/,
        loader: "babel-loader",
        exclude: /node_modules/,
        options: {
          presets: ["@babel/preset-react"],
          plugins: [["@babel/plugin-proposal-class-properties"]],
        },
      },
      {
        test: /\.(s[ac]ss|css)$/,
        use: ["css-loader", "sass-loader"],
      },
      {
        test: /\.(png|jpg|svg|gif|eot|ttf|woff)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: "file-loader",
      },
    ],
  },
};
